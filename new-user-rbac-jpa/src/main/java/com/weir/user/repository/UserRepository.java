package com.weir.user.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.weir.user.entity.PermUser;

@Component
public interface UserRepository extends JpaRepository<PermUser, Integer> {
	PermUser findByUsername(String username);

	Optional<PermUser> findById(Integer id);
}