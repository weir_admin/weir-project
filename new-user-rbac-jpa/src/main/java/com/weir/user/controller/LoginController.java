package com.weir.user.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import lombok.extern.slf4j.Slf4j;

@Slf4j
//@Tag(name = "登录管理")
@Controller
@RequestMapping("/user")
public class LoginController {

//	@Operation(summary = "登录")
	@RequestMapping("/login-ui")
	public String login() {
		return "login";
	}

}
