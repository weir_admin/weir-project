package com.weir.user.entity;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import jakarta.persistence.JoinColumn;
import lombok.Data;

/**
 * <p>
 * 菜单
 * </p>
 *
 * @author weir
 * @since 2018-12-24
 */
@Entity
@Table(name = "perm_menu")
@Data
public class PermMenu {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 模块名称
     */
    private String text;

    /**
     * 模块编码
     */
    private String code;

    /**
     * 模块路径
     */
    private String url;

    /**
     * 父级ID
     */
    private Integer pid;

    /**
     * 级别(1菜单和2功能)
     */
    private Integer step;

    /**
     * 备注
     */
    private String remark;

//    @ManyToMany(fetch = FetchType.LAZY,mappedBy = "permissions")
//    private List<PermRole> roles;
}
