package com.weir.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@SpringBootApplication
@EnableJpaRepositories(value = "com.weir.order.repository")
public class OrderJPAApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderJPAApplication.class, args);
	}

}
