package com.weir.order.entity;

import java.util.Date;

import org.hibernate.annotations.BatchSize;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

/**
 * 订单子表
 * @author weir
 *
 */
@BatchSize(size = 100)
@Entity
@Table(name = "order_item")
public class OrderItem {
	
	private static final long serialVersionUID = -7813409518159851507L;

	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long id;
	@Column(name = "order_id")
	public Long orderId;

	@Column(name = "order_no")
	public String orderNo;

	@Column(name = "member_id")
    public Integer memberId;

	@Column(name = "good_id")
    public Integer goodId;

	@Column(name = "good_money")
    public Double goodMoney;
	
//	@ApiModelProperty(value = "订单编号")
//	@Column(name = "order_no")
//    public String orderSn;

//    @ApiModelProperty(value = "子订单编号")
	@Column(name = "order_item_sn")
    public String orderItemSn;

//    @ApiModelProperty(value = "单价")
	@Column(name = "unit_price")
    public Double unitPrice;

//    @ApiModelProperty(value = "小记")
    @Column(name = "sub_total")
    public Double subTotal;

//    @ApiModelProperty(value = "商品ID")
    @Column(name = "goods_id")
    public String goodsId;

//    @ApiModelProperty(value = "货品ID")
    @Column(name = "sku_id")
    public String skuId;

//    @ApiModelProperty(value = "销售量")
    @Column(name = "num")
    public Integer num;

//    @ApiModelProperty(value = "交易编号")
    @Column(name = "trade_sn")
    public String tradeSn;

//    @ApiModelProperty(value = "图片")
//    @Column(name = "trade_sn")
    public String image;

//    @ApiModelProperty(value = "商品名称")
    @Column(name = "goods_name")
    public String goodsName;

//    @ApiModelProperty(value = "分类ID")
    @Column(name = "category_id")
    public String categoryId;

//    @ApiModelProperty(value = "快照id")
    @Column(name = "snapshot_id")
    public String snapshotId;

//    @ApiModelProperty(value = "规格json")
//    @Column(name = "snapshot_id")
    public String specs;

//    @ApiModelProperty(value = "促销类型")
    @Column(name = "promotion_type")
    public String promotionType;

//    @ApiModelProperty(value = "促销id")
    @Column(name = "promotion_id")
    public String promotionId;

//    @ApiModelProperty(value = "销售金额")
    @Column(name = "goods_price")
    public Double goodsPrice;

//    @ApiModelProperty(value = "实际金额")
    @Column(name = "flow_price")
    public Double flowPrice;

    /**
     * @see CommentStatusEnum
     */
//    @ApiModelProperty(value = "评论状态:未评论(UNFINISHED),待追评(WAIT_CHASE),评论完成(FINISHED)，")
    @Column(name = "comment_status")
    public String commentStatus;

    /**
     * @see OrderItemAfterSaleStatusEnum
     */
//    @ApiModelProperty(value = "售后状态")
    @Column(name = "after_sale_status")
    public String afterSaleStatus;

//    @ApiModelProperty(value = "价格详情")
    @Column(name = "price_detail")
    public String priceDetail;

    /**
     * @see OrderComplaintStatusEnum
     */
//    @ApiModelProperty(value = "投诉状态")
    @Column(name = "complain_detail")
    public String complainStatus;

//    @ApiModelProperty(value = "交易投诉id")
    @Column(name = "complain_id")
    public String complainId;

//    @ApiModelProperty(value = "退货商品数量")
    @Column(name = "return_goods_number")
    public Integer returnGoodsNumber;
    public Integer creator;
    public Date createTime;

}
