package com.weir.order.entity;

import java.util.Date;

import org.hibernate.annotations.BatchSize;

import com.fasterxml.jackson.annotation.JsonFormat;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

/**
 * 订单表
 * @author weir
 *
 */
@BatchSize(size = 100)
@Entity
@Table(name = "order_info")
public class OrderInfo {
	
	private static final long serialVersionUID = 5582127649774711369L;
	
	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long id;

	@Column(name = "order_no")
	public String orderNo;

	@Column(name = "member_id")
    public String memberId;

	@Column(name = "order_count")
    public Integer orderCount;
// 总价格
	@Column(name = "order_money")
    public Double orderMoney;
	
//	@ApiModelProperty("订单编号")
//    public String sn;

//    @ApiModelProperty("交易编号 关联Trade")
    @Column(name = "trade_sn")
    public String tradeSn;

//    @ApiModelProperty(value = "店铺ID")
    @Column(name = "store_id")
    public String storeId;

//    @ApiModelProperty(value = "店铺名称")
    @Column(name = "store_name")
    public String storeName;


//    @ApiModelProperty(value = "用户名")
//    @Sensitive(strategy = SensitiveStrategy.PHONE)
    @Column(name = "member_name")
    public String memberName;

    /**
     * @see OrderStatusEnum
     */
//    @ApiModelProperty(value = "订单状态")
    @Column(name = "order_status")
    public String orderStatus;

    /**
     * @see PayStatusEnum
     */
//    @ApiModelProperty(value = "付款状态")
    @Column(name = "pay_status")
    public String payStatus;
    /**
     * @see DeliverStatusEnum
     */
//    @ApiModelProperty(value = "货运状态")
    @Column(name = "deliver_status")
    public String deliverStatus;

//    @ApiModelProperty(value = "第三方付款流水号")
    @Column(name = "receivable_no")
    public String receivableNo;

    /**
     * @see  PaymentMethodEnum
     */
//    @ApiModelProperty(value = "支付方式")
    @Column(name = "payment_method")
    public String paymentMethod;

//    @ApiModelProperty(value = "支付时间")
//    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "payment_time")
    public Date paymentTime;

//    @ApiModelProperty(value = "收件人姓名")
    @Column(name = "consignee_name")
    public String consigneeName;

//    @ApiModelProperty(value = "收件人手机")
    @Column(name = "consignee_mobile")
    public String consigneeMobile;

    /**
     * @see DeliveryMethodEnum
     */
//    @ApiModelProperty(value = "配送方式")
    @Column(name = "delivery_method")
    public String deliveryMethod;

//    @ApiModelProperty(value = "地址名称， '，'分割")
    @Column(name = "consignee_address_path")
    public String consigneeAddressPath;

//    @ApiModelProperty(value = "地址id，'，'分割 ")
    @Column(name = "consignee_address_id_path")
    public String consigneeAddressIdPath;

//    @ApiModelProperty(value = "详细地址")
    @Column(name = "consignee_detail")
    public String consigneeDetail;

////    @ApiModelProperty(value = "总价格")
//    @Column(name = "flowPrice")
//    public Double flowPrice;

//    @ApiModelProperty(value = "商品价格")
    @Column(name = "goods_price")
    public Double goodsPrice;

//    @ApiModelProperty(value = "运费(分)")
    @Column(name = "freight_price")
    public Double freightPrice;

//    @ApiModelProperty(value = "优惠的金额")
    @Column(name = "discount_price")
    public Double discountPrice;

//    @ApiModelProperty(value = "修改价格")
    @Column(name = "update_price")
    public Double updatePrice;

//    @ApiModelProperty(value = "发货单号")
    @Column(name = "logistics_no")
    public String logisticsNo;

//    @ApiModelProperty(value = "物流公司CODE")
    @Column(name = "logistics_code")
    public String logisticsCode;

//    @ApiModelProperty(value = "物流公司名称")
    @Column(name = "logistics_name")
    public String logisticsName;

//    @ApiModelProperty(value = "订单商品总重量")
    public Double weight;

//    @ApiModelProperty(value = "商品数量")
    @Column(name = "goods_num")
    public Integer goodsNum;

//    @ApiModelProperty(value = "买家订单备注")
//    public String remark;

//    @ApiModelProperty(value = "订单取消原因")
    @Column(name = "cancel_reason")
    public String cancelReason;

//    @ApiModelProperty(value = "完成时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "complete_time")
    public Date completeTime;

//    @ApiModelProperty(value = "送货时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "logistics_time")
    public Date logisticsTime;

//    @ApiModelProperty(value = "支付方式返回的交易号")
    @Column(name = "pay_order_no")
    public String payOrderNo;

    /**
     * @see ClientTypeEnum
     */
//    @ApiModelProperty(value = "订单来源")
    @Column(name = "client_type")
    public String clientType;

//    @ApiModelProperty(value = "是否需要发票")
    @Column(name = "need_receipt")
    public Boolean needReceipt;

//    @ApiModelProperty(value = "是否为其他订单下的订单，如果是则为依赖订单的sn，否则为空")
    @Column(name = "parent_order_sn")
    public String parentOrderSn = "";

//    @ApiModelProperty(value = "是否为某订单类型的订单，如果是则为订单类型的id，否则为空")
    @Column(name = "promotion_id")
    public String promotionId;

    /**
     * @see OrderTypeEnum
     */
//    @ApiModelProperty(value = "订单类型")
    @Column(name = "order_type")
    public String orderType;

    /**
     * @see OrderPromotionTypeEnum
     */
//    @ApiModelProperty(value = "订单促销类型")
    @Column(name = "order_promotion_type")
    public String orderPromotionType;

//    @ApiModelProperty(value = "价格价格详情")
    @Column(name = "price_detail")
    public String priceDetail;

//    @ApiModelProperty(value = "订单是否支持原路退回")
    @Column(name = "can_return")
    public Boolean canReturn;

//    @ApiModelProperty(value = "提货码")
    @Column(name = "verification_code")
    public String verificationCode;

//    @ApiModelProperty(value = "分销员ID")
    @Column(name = "distribution_id")
    public String distributionId;

//    @ApiModelProperty(value = "使用的店铺会员优惠券id(,区分)")
    @Column(name = "use_store_member_coupon_ids")
    public String useStoreMemberCouponIds;

//    @ApiModelProperty(value = "使用的平台会员优惠券id")
    @Column(name = "use_platform_member_coupon_ids")
    public String usePlatformMemberCouponId;

//    @ApiModelProperty(value = "qrCode  实物为提货码  虚拟货物为账号")
    @Column(name = "qr_code")
    public String qrCode;

//    @ApiModelProperty(value = "自提点地址")
    @Column(name = "store_address_path")
    public String storeAddressPath;

//    @ApiModelProperty(value = "自提点电话")
    @Column(name = "store_address_mobile")
    public String storeAddressMobile;

//    @ApiModelProperty(value = "自提点地址经纬度")
    @Column(name = "store_address_center")
    public String storeAddressCenter;
}
