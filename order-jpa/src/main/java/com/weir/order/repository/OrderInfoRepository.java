package com.weir.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.weir.order.entity.OrderInfo;

@Component
public interface OrderInfoRepository extends JpaRepository<OrderInfo, Long> {
}