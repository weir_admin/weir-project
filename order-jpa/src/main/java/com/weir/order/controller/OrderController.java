package com.weir.order.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.weir.order.entity.OrderInfo;
import com.weir.order.entity.OrderItem;
import com.weir.order.repository.OrderInfoRepository;
import com.weir.order.service.BatchService;
import com.weir.order.utils.ChineseName;
import com.weir.order.utils.OrderNoCreate;
import com.weir.order.utils.RandomPhoneNumGenerator;
import com.weir.order.utils.SFExpressNumberGenerator;
import com.weir.order.utils.Sequence;

@RestController
@RequestMapping("/order")
public class OrderController {

	@Autowired
	private OrderInfoRepository orderInfoRepository;
	@Autowired
	private BatchService batchService;
	
	private final Sequence sequence = new Sequence(null);
	
	@GetMapping("/list-page")
	public Page<OrderInfo> listPage(@RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
		Page<OrderInfo> findAll = orderInfoRepository.findAll(PageRequest.of(pageNo - 1, pageSize));
		return findAll;
	}
	
	@GetMapping("/save-batch")
	public String saveBatch() {
		long a = System.currentTimeMillis();
		List<OrderInfo> orderInfos = new ArrayList<>();
		List<OrderItem> orderItems = new ArrayList<>();
		for (int i = 0; i < 10000; i++) {
			OrderInfo orderInfo = new OrderInfo();
			orderInfo.id = sequence.nextId();
			orderInfo.clientType = "PC";
			orderInfo.consigneeAddressPath = "龙城镇钮王村";
			orderInfo.consigneeDetail = "龙城镇钮王村";
			orderInfo.consigneeMobile = RandomPhoneNumGenerator.generatePhoneNum();
			orderInfo.consigneeName = ChineseName.getName();
			orderInfo.deliverStatus = "UNDELIVERED";
			orderInfo.deliveryMethod = "LOGISTICS";
			orderInfo.discountPrice = 30d;
			orderInfo.freightPrice = 600d;
			orderInfo.goodsNum = 2;
			orderInfo.goodsPrice = 21.3;
			orderInfo.logisticsCode = "SF";
			orderInfo.logisticsName = "顺丰快递";
			orderInfo.logisticsNo = SFExpressNumberGenerator.generateSFExpressNumber();
			orderInfo.logisticsTime = new Date();
			orderInfo.memberId = sequence.nextId().toString();
			orderInfo.memberName = ChineseName.getName();
			orderInfo.needReceipt = false;
			orderInfo.orderCount = 4;
			orderInfo.orderMoney = 45.5;
			orderInfo.orderNo = OrderNoCreate.randomOrderCode();
			orderInfo.orderPromotionType = "NORMAL";
			orderInfo.orderStatus = "UNDELIVERED";
			orderInfo.orderType = "NORMAL";
			orderInfo.paymentMethod = "WECHAT";
			orderInfo.paymentTime = new Date();
			orderInfo.payStatus = "UNPAID";
			orderInfo.storeName = "路口商店";
			orderInfos.add(orderInfo);
			for (int j = 0; j < 3; j++) {
				OrderItem orderItem = new OrderItem();
				orderItem.id = sequence.nextId();
				orderItem.afterSaleStatus = "NOT_APPLIED";
				orderItem.categoryId = Integer.valueOf(i+1).toString();
				orderItem.createTime = new Date();
				orderItem.creator = i+1;
				orderItem.flowPrice = 44.5;
				orderItem.goodId = i+1;
				orderItem.goodMoney = 33.5;
				orderItem.goodsName = "冰城雪梨";
				orderItem.goodsPrice = 4.5;
				orderItem.image = "xxx/xxxxxxx/xxxxxxx.jpg";
				orderItem.memberId = i+1;
				orderItem.num = 2;
				orderItem.orderId = orderInfo.id;
				orderItem.orderItemSn = orderInfo.orderNo + j + 1;
				orderItems.add(orderItem);
			}
		}
		batchService.batchInsert(orderInfos);
		batchService.batchInsert(orderItems);
		long b = System.currentTimeMillis();
		return "耗时："+ (b - a);
	}
}
