package com.weir.user.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.weir.user.entity.PermUserDTO;

@FeignClient(value = "new-user-rbac-jpa",path = "/user")
@Component
public interface UserServiceFeign {

	@GetMapping("/get/{username}")
	PermUserDTO getByName(@PathVariable("username") String username);

	@GetMapping("/{id}")
	public PermUserDTO get(@PathVariable("id") Integer id);
}
