package com.weir.user.entity;

import java.util.List;

import lombok.Data;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
@Data
public class PermUserDTO {

    private Integer id;

    private String username;

    private String pwd;

    private List<String> permCodes;  
}
