package com.weir.user.vo;

import lombok.Data;

@Data
public class LoginVo {

	private String userName;
	private String pwd;
}
