package com.weir.user.vo;

public class ApiRespResult<T> {

	private int code = 200;
	private String msg;
	private T data;
	
	public ApiRespResult() {
	}

	public ApiRespResult(T data) {
		this.data = data;
	}

	public ApiRespResult(String msg) {
		this.msg = msg;
	}

	public static ApiRespResult<Object> error(String msg) {
		return new ApiRespResult<>(msg);
	}
}
