package com.weir.user.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.weir.user.entity.PermUser;

@FeignClient(value = "new-user-rbac")
@Component
public interface UserServiceFeign {

	@GetMapping("/user/get/{username}")
	PermUser getByName(@PathVariable("username") String username);
}
