package com.weir.user.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 用户角色表
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
@TableName(value="perm_user_role")
public class PermUserRole extends Model<PermUserRole> {

    private static final long serialVersionUID = 1L;

    @TableId()
    private Integer id;
    private Integer userId;

    private Integer roleId;

    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    @Override
	public Serializable pkVal() {
        return this.userId;
    }

    @Override
    public String toString() {
        return "PermUserRole{" +
        "userId=" + userId +
        ", roleId=" + roleId +
        "}";
    }
}
