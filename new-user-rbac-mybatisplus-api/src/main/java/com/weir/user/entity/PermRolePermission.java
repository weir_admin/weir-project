package com.weir.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 角色权限表
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
@TableName(value="perm_role_permission")
public class PermRolePermission extends Model<PermRolePermission> {

    private static final long serialVersionUID = 1L;

    @TableId(type=IdType.AUTO)
    private Integer id;
    private Integer permissionId;

    private Integer roleId;

    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PermRolePermission() {
		super();
	}

	public PermRolePermission(Integer permissionId, Integer roleId) {
		super();
		this.permissionId = permissionId;
		this.roleId = roleId;
	}

	public Integer getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    @Override
	public Serializable pkVal() {
        return this.id;
    }

	@Override
	public String toString() {
		return "PermRolePermission [id=" + id + ", permissionId=" + permissionId + ", roleId=" + roleId + "]";
	}

}
