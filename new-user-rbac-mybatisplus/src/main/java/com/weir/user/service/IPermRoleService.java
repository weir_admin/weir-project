package com.weir.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.weir.user.entity.PermRole;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
public interface IPermRoleService extends IService<PermRole> {

}
