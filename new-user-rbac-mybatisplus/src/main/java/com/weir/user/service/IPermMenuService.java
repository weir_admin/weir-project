package com.weir.user.service;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.weir.user.entity.PermMenu;

/**
 * <p>
 * 菜单 服务类
 * </p>
 *
 * @author weir
 * @since 2018-12-24
 */
public interface IPermMenuService extends IService<PermMenu> {


}
