package com.weir.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.weir.user.entity.PermUser;
import com.weir.user.mapper.PermUserMapper;
import com.weir.user.service.IPermUserService;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
@Service
public class PermUserServiceImpl extends ServiceImpl<PermUserMapper, PermUser> implements IPermUserService {

	@Autowired
	private PermUserMapper permUserMapper;
	
	
}
