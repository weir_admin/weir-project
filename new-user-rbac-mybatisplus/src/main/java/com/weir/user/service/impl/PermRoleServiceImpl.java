package com.weir.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.weir.user.entity.PermRole;
import com.weir.user.mapper.PermRoleMapper;
import com.weir.user.mapper.PermRolePermissionMapper;
import com.weir.user.service.IPermRoleService;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
@Service
public class PermRoleServiceImpl extends ServiceImpl<PermRoleMapper, PermRole> implements IPermRoleService {
	@Autowired
	private PermRolePermissionMapper permRolePermissionMapper;
	@Autowired
	private PermRoleMapper permRoleMapper;

}
