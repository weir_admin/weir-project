package com.weir.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.weir.user.entity.PermMenu;
import com.weir.user.mapper.PermMenuMapper;
import com.weir.user.service.IPermMenuService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 菜单 服务实现类
 * </p>
 *
 * @author weir
 * @since 2018-12-24
 */
@Service
public class PermMenuServiceImpl extends ServiceImpl<PermMenuMapper, PermMenu> implements IPermMenuService {
	
}
