package com.weir.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.weir.user.entity.PermUserRole;
import com.weir.user.mapper.PermUserRoleMapper;
import com.weir.user.service.IPermUserRoleService;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
@Service
public class PermUserRoleServiceImpl extends ServiceImpl<PermUserRoleMapper, PermUserRole> implements IPermUserRoleService {

}
