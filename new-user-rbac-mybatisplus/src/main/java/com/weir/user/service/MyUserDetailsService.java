//package com.weir.user.service;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//
//import com.weir.user.entity.PermUser;
//import com.weir.user.repository.UserRepository;
//
//@Service
//public class MyUserDetailsService implements UserDetailsService {
// 
//    @Autowired
//    private UserRepository userRepository;
// 
//    @Override
//    public UserDetails loadUserByUsername(String username) {
//        PermUser user = userRepository.findByUsername(username);
//        if (user == null) {
//            throw new UsernameNotFoundException(username);
//        }
//        return new MyUserPrincipal(user);
//    }
//}
