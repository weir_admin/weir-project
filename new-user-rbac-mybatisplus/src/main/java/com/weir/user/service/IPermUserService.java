package com.weir.user.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.weir.user.entity.PermUser;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
public interface IPermUserService extends IService<PermUser> {

}
