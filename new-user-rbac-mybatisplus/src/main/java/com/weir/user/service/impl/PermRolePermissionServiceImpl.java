package com.weir.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.weir.user.entity.PermRolePermission;
import com.weir.user.mapper.PermRolePermissionMapper;
import com.weir.user.service.IPermRolePermissionService;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色权限表 服务实现类
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
@Service
public class PermRolePermissionServiceImpl extends ServiceImpl<PermRolePermissionMapper, PermRolePermission> implements IPermRolePermissionService {

}
