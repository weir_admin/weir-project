package com.weir.user.mapper;

import java.util.Set;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.weir.user.entity.PermMenu;
import com.weir.user.entity.PermUser;
import com.weir.user.entity.PermUserRole;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
public interface PermUserMapper extends BaseMapper<PermUser> {

	Set<String> selectRoleNameByUserId(@Param("userId") Integer userId);
	
	Set<String> selectPermNameByUserId(@Param("userId") Integer userId);
	
	String getUserRoleCodes(@Param("ew") Wrapper<PermUserRole> wrapper);
	String getUserModuleCodes(@Param("ew") Wrapper<PermMenu> wrapper);
}
