package com.weir.user.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.weir.user.entity.PermMenu;

/**
 * <p>
 * 菜单 Mapper 接口
 * </p>
 *
 * @author weir
 * @since 2018-12-24
 */
public interface PermMenuMapper extends BaseMapper<PermMenu> {
	List<PermMenu> getModule(@Param("ew") Wrapper<PermMenu> wrapper);
	/**
	 * 获取用户权限
	 * @param userId 用户ID
	 * @return
	 */
	List<PermMenu> selectByUserId(@Param("userId") Integer userId);
}
