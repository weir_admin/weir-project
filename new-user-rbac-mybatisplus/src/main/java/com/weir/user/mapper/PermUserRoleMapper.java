package com.weir.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.weir.user.entity.PermUserRole;

/**
 * <p>
 * 用户角色表 Mapper 接口
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
public interface PermUserRoleMapper extends BaseMapper<PermUserRole> {

}
