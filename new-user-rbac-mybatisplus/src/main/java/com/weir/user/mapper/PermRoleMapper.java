package com.weir.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.weir.user.entity.PermRole;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
public interface PermRoleMapper extends BaseMapper<PermRole> {

    List<String> selectCodeByUserId(@Param("UserId") Integer UserId);
}
