package com.weir.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.weir.user.entity.PermUser;
import com.weir.user.service.UserServiceFeign;

@RestController
@RequestMapping("/user")
public class UserController implements UserServiceFeign{

	
	@GetMapping("/{id}")
	public PermUser get(@PathVariable("id") Integer id) {
		return null;
	}
	
	@GetMapping("get/{username}")
	public PermUser getByName(@PathVariable("username") String username) {
		return null;
	}
}
