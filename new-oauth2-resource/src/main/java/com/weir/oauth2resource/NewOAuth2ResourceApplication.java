package com.weir.oauth2resource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewOAuth2ResourceApplication {
    public static void main(String[] args) {
        SpringApplication.run(NewOAuth2ResourceApplication.class, args);
    }
}
