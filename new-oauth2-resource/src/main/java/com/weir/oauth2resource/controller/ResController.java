package com.weir.oauth2resource.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 资源控制器
 */
@RestController
public class ResController {
    /**
     * 受保护的资源
     * @return
     */
    @GetMapping("res")
    public String getRes() {
        return "成功获取到受保护的资源";
    }

    /**
     * 通过注解控制权限
     * @return
     */
    @GetMapping("res2")
    @PreAuthorize("hasRole('ADMIN')")
    public String getRes2() {
        return "hasRole('ADMIN'+成功获取到受保护的资源";
    }
    @GetMapping("res3")
    @PreAuthorize("hasAuthority('res-perm')")
    public String getRes3() {
    	return "hasAuthority('res-perm')+成功获取到受保护的资源";
    }
    @GetMapping("res4")
    @PreAuthorize("hasAuthority('res-perm4')")
    public String getRes4() {
    	return "hasAuthority('res-perm4')+成功获取到受保护的资源";
    }
    @GetMapping("res5")
    @PreAuthorize("hasAuthority('resource-get')")
    public String getRes5() {
    	return "hasAuthority('resource-get')+成功获取到受保护的资源";
    }
    @GetMapping("res6")
    @PreAuthorize("hasAuthority('user_edit')")
    public String getRes6() {
    	return "hasAuthority('user_edit')+成功获取到受保护的资源";
    }
    @GetMapping("res7")
    @PreAuthorize("hasAuthority('user_add')")
    public String getRes7() {
    	return "hasAuthority('user_add')+成功获取到受保护的资源";
    }
}
