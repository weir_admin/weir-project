package com.weir.sas.oauth2.mp.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author weir
 * @since 2022-12-31
 */
@Getter
@Setter
@TableName("oauth2_client_setting")
public class Oauth2ClientSetting extends Model<Oauth2ClientSetting> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String clientId;

    private Boolean requireAuthorizationConsent;

    private LocalDateTime createdDate;

    private Long createdBy;

    private LocalDateTime updatedDate;

    private Long updatedBy;

    private LocalDateTime deletedDate;

    @Override
    public Serializable pkVal() {
        return this.id;
    }
}
