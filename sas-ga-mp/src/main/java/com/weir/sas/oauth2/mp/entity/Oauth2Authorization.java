package com.weir.sas.oauth2.mp.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author weir
 * @since 2022-12-31
 */
@Getter
@Setter
@TableName("oauth2_authorization")
public class Oauth2Authorization extends Model<Oauth2Authorization> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    private String registeredClientId;

    private String principalName;

    private String authorizationGrantType;

    private String authorizedScopes;

    private byte[] attributes;

    private String state;

    private byte[] authorizationCodeValue;

    private LocalDateTime authorizationCodeIssuedAt;

    private LocalDateTime authorizationCodeExpiresAt;

    private byte[] authorizationCodeMetadata;

    private byte[] accessTokenValue;

    private LocalDateTime accessTokenIssuedAt;

    private LocalDateTime accessTokenExpiresAt;

    private byte[] accessTokenMetadata;

    private String accessTokenType;

    private String accessTokenScopes;

    private byte[] oidcIdTokenValue;

    private LocalDateTime oidcIdTokenIssuedAt;

    private LocalDateTime oidcIdTokenExpiresAt;

    private byte[] oidcIdTokenMetadata;

    private byte[] refreshTokenValue;

    private LocalDateTime refreshTokenIssuedAt;

    private LocalDateTime refreshTokenExpiresAt;

    private byte[] refreshTokenMetadata;

    @Override
    public Serializable pkVal() {
        return this.id;
    }
}
