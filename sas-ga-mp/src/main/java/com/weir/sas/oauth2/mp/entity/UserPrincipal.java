package com.weir.sas.oauth2.mp.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author weir
 * @since 2022-12-31
 */
@Getter
@Setter
public class UserPrincipal extends Model<UserPrincipal> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "UserId", type = IdType.AUTO)
    private Long userId;

    private String username;

    private String hashedPassword;

    private Boolean accountNonExpired;

    private Boolean accountNonLocked;

    private Boolean credentialsNonExpired;

    private Boolean enabled;

    private LocalDateTime createdDate;

    private Long createdBy;

    private LocalDateTime updatedDate;

    private Long updatedBy;

    private LocalDateTime deletedDate;

    @Override
    public Serializable pkVal() {
        return this.userId;
    }
}
