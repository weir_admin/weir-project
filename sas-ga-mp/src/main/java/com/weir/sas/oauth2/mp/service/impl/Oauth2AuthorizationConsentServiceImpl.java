package com.weir.sas.oauth2.mp.service.impl;

import com.weir.sas.oauth2.mp.entity.Oauth2AuthorizationConsent;
import com.weir.sas.oauth2.mp.mapper.Oauth2AuthorizationConsentMapper;
import com.weir.sas.oauth2.mp.service.IOauth2AuthorizationConsentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author weir
 * @since 2022-12-31
 */
@Service
public class Oauth2AuthorizationConsentServiceImpl extends ServiceImpl<Oauth2AuthorizationConsentMapper, Oauth2AuthorizationConsent> implements IOauth2AuthorizationConsentService {

}
