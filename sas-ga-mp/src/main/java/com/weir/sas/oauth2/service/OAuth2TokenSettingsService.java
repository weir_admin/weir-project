package com.weir.sas.oauth2.service;

import org.springframework.security.oauth2.server.authorization.settings.TokenSettings;

import com.weir.sas.oauth2.mp.entity.Oauth2ClientTokenSetting;

public interface OAuth2TokenSettingsService {

	TokenSettings getTokenSettings(Oauth2ClientTokenSetting clientTokenSetting);
	
}
