package com.weir.sas.oauth2.mp.service.impl;

import com.weir.sas.oauth2.mp.entity.UserPrincipal;
import com.weir.sas.oauth2.mp.mapper.UserPrincipalMapper;
import com.weir.sas.oauth2.mp.service.IUserPrincipalService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author weir
 * @since 2022-12-31
 */
@Service
public class UserPrincipalServiceImpl extends ServiceImpl<UserPrincipalMapper, UserPrincipal> implements IUserPrincipalService {

}
