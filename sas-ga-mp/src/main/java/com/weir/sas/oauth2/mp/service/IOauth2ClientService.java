package com.weir.sas.oauth2.mp.service;

import com.weir.sas.oauth2.mp.entity.Oauth2Client;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author weir
 * @since 2022-12-31
 */
public interface IOauth2ClientService extends IService<Oauth2Client> {

}
