package com.weir.sas.oauth2.mp.service.impl;

import com.weir.sas.oauth2.mp.entity.Oauth2Client;
import com.weir.sas.oauth2.mp.mapper.Oauth2ClientMapper;
import com.weir.sas.oauth2.mp.service.IOauth2ClientService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author weir
 * @since 2022-12-31
 */
@Service
public class Oauth2ClientServiceImpl extends ServiceImpl<Oauth2ClientMapper, Oauth2Client> implements IOauth2ClientService {

}
