package com.weir.sas.oauth2.mp.service.impl;

import com.weir.sas.oauth2.mp.entity.Oauth2Authorization;
import com.weir.sas.oauth2.mp.mapper.Oauth2AuthorizationMapper;
import com.weir.sas.oauth2.mp.service.IOauth2AuthorizationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author weir
 * @since 2022-12-31
 */
@Service
public class Oauth2AuthorizationServiceImpl extends ServiceImpl<Oauth2AuthorizationMapper, Oauth2Authorization> implements IOauth2AuthorizationService {

}
