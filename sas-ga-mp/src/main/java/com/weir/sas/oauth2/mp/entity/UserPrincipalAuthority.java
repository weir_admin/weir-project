package com.weir.sas.oauth2.mp.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author weir
 * @since 2022-12-31
 */
@Getter
@Setter
public class UserPrincipalAuthority extends Model<UserPrincipalAuthority> {

    private static final long serialVersionUID = 1L;

    private Long userId;

    private String authority;

    @Override
    public Serializable pkVal() {
        return null;
    }
}
