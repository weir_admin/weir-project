package com.weir.sas.oauth2.mp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.weir.sas.oauth2.mp.entity.UserPrincipalAuthority;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author weir
 * @since 2022-12-31
 */
public interface UserPrincipalAuthorityMapper extends BaseMapper<UserPrincipalAuthority> {

}
