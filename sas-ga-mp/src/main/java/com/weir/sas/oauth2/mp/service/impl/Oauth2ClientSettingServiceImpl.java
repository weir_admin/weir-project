package com.weir.sas.oauth2.mp.service.impl;

import com.weir.sas.oauth2.mp.entity.Oauth2ClientSetting;
import com.weir.sas.oauth2.mp.mapper.Oauth2ClientSettingMapper;
import com.weir.sas.oauth2.mp.service.IOauth2ClientSettingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import org.springframework.security.oauth2.server.authorization.settings.ClientSettings;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author weir
 * @since 2022-12-31
 */
@Service
public class Oauth2ClientSettingServiceImpl extends ServiceImpl<Oauth2ClientSettingMapper, Oauth2ClientSetting> implements IOauth2ClientSettingService {

	@Override
	public ClientSettings getClientSettings(Oauth2ClientSetting clientSetting) {
		if (clientSetting == null) {
			return null;
		}
		boolean requireAuthorizationConsent = clientSetting.getRequireAuthorizationConsent();
		ClientSettings clientSettings = ClientSettings.builder().requireAuthorizationConsent(requireAuthorizationConsent).build();
		return clientSettings;
	}
}
