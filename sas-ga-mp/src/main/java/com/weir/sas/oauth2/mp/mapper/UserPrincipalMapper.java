package com.weir.sas.oauth2.mp.mapper;

import com.weir.sas.oauth2.mp.entity.UserPrincipal;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author weir
 * @since 2022-12-31
 */
public interface UserPrincipalMapper extends BaseMapper<UserPrincipal> {

}
