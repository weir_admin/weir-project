package com.weir.sas.oauth2.mp.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author weir
 * @since 2022-12-31
 */
@Getter
@Setter
@TableName("oauth2_authorization_consent")
public class Oauth2AuthorizationConsent extends Model<Oauth2AuthorizationConsent> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "registered_client_id", type = IdType.ASSIGN_ID)
    private String registeredClientId;

    @TableId(value = "principal_name", type = IdType.ASSIGN_ID)
    private String principalName;

    private String authorities;

    @Override
    public Serializable pkVal() {
        return this.principalName;
    }
}
