package com.weir.sas.oauth2.mp.service;

import com.weir.sas.oauth2.mp.entity.Oauth2ClientSetting;

import org.springframework.security.oauth2.server.authorization.settings.ClientSettings;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author weir
 * @since 2022-12-31
 */
public interface IOauth2ClientSettingService extends IService<Oauth2ClientSetting> {

	ClientSettings getClientSettings(Oauth2ClientSetting clientSetting);

}
