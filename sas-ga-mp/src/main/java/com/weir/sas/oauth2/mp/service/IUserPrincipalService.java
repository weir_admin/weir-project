package com.weir.sas.oauth2.mp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.weir.sas.oauth2.mp.entity.UserPrincipal;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author weir
 * @since 2022-12-31
 */
public interface IUserPrincipalService extends IService<UserPrincipal> {

}
