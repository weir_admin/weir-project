package com.weir.sas.oauth2.mp.service.impl;

import com.weir.sas.oauth2.mp.entity.UserPrincipalAuthority;
import com.weir.sas.oauth2.mp.mapper.UserPrincipalAuthorityMapper;
import com.weir.sas.oauth2.mp.service.IUserPrincipalAuthorityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author weir
 * @since 2022-12-31
 */
@Service
public class UserPrincipalAuthorityServiceImpl extends ServiceImpl<UserPrincipalAuthorityMapper, UserPrincipalAuthority> implements IUserPrincipalAuthorityService {

}
