package com.weir.sas.oauth2.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.security.jackson2.SecurityJackson2Modules;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.jackson2.OAuth2AuthorizationServerJackson2Module;
import org.springframework.security.oauth2.server.authorization.settings.ClientSettings;
import org.springframework.security.oauth2.server.authorization.settings.TokenSettings;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.weir.sas.oauth2.OAuth2Util;
import com.weir.sas.oauth2.mp.entity.Oauth2RegisteredClient;
import com.weir.sas.oauth2.mp.mapper.Oauth2RegisteredClientMapper;

// This is registered as spring bean in OAuth2RegisteredClientConfiguration. No need to use @component
public class MpRegisteredClientRepository implements RegisteredClientRepository {

	private final Oauth2RegisteredClientMapper oauth2RegisteredClientMapper;
	private final ObjectMapper objectMapper = new ObjectMapper();

	public MpRegisteredClientRepository(Oauth2RegisteredClientMapper oauth2RegisteredClientMapper) {
		Assert.notNull(oauth2RegisteredClientMapper, "oauth2RegisteredClientMapper cannot be null");
		this.oauth2RegisteredClientMapper = oauth2RegisteredClientMapper;

		ClassLoader classLoader = MpRegisteredClientRepository.class.getClassLoader();
		List<Module> securityModules = SecurityJackson2Modules.getModules(classLoader);
		this.objectMapper.registerModules(securityModules);
		this.objectMapper.registerModule(new OAuth2AuthorizationServerJackson2Module());
	}

	@Override
	public void save(RegisteredClient registeredClient) {
		Assert.notNull(registeredClient, "registeredClient cannot be null");
		this.oauth2RegisteredClientMapper.insert(toEntity(registeredClient));
	}

	@Override
	public RegisteredClient findById(String id) {
		Assert.hasText(id, "id cannot be empty");
		Oauth2RegisteredClient oauth2RegisteredClient = this.oauth2RegisteredClientMapper.selectById(id);
		return toObject(oauth2RegisteredClient);
	}

	@Override
	public RegisteredClient findByClientId(String clientId) {
		Assert.hasText(clientId, "clientId cannot be empty");
		Oauth2RegisteredClient oauth2RegisteredClient = this.oauth2RegisteredClientMapper.selectOne(new QueryWrapper<Oauth2RegisteredClient>().eq("client_id", clientId));
		return toObject(oauth2RegisteredClient);
	}

	private RegisteredClient toObject(Oauth2RegisteredClient oauth2RegisteredClient) {
		Set<String> clientAuthenticationMethods = StringUtils.commaDelimitedListToSet(
				oauth2RegisteredClient.getClientAuthenticationMethods());
		Set<String> authorizationGrantTypes = StringUtils.commaDelimitedListToSet(
				oauth2RegisteredClient.getAuthorizationGrantTypes());
		Set<String> redirectUris = StringUtils.commaDelimitedListToSet(
				oauth2RegisteredClient.getRedirectUris());
		Set<String> clientScopes = StringUtils.commaDelimitedListToSet(
				oauth2RegisteredClient.getScopes());

		RegisteredClient.Builder builder = RegisteredClient.withId(oauth2RegisteredClient.getId())
				.clientId(oauth2RegisteredClient.getClientId())
				.clientIdIssuedAt(oauth2RegisteredClient.getClientIdIssuedAt())
				.clientSecret(oauth2RegisteredClient.getClientSecret())
				.clientSecretExpiresAt(oauth2RegisteredClient.getClientSecretExpiresAt())
				.clientName(oauth2RegisteredClient.getClientName())
				.clientAuthenticationMethods(authenticationMethods ->
						clientAuthenticationMethods.forEach(authenticationMethod ->
								authenticationMethods.add(OAuth2Util.resolveClientAuthenticationMethod(authenticationMethod))))
				.authorizationGrantTypes((grantTypes) ->
						authorizationGrantTypes.forEach(grantType ->
								grantTypes.add(OAuth2Util.resolveAuthorizationGrantType(grantType))))
				.redirectUris((uris) -> uris.addAll(redirectUris))
				.scopes((scopes) -> scopes.addAll(clientScopes));

		Map<String, Object> clientSettingsMap = parseMap(oauth2RegisteredClient.getClientSettings());
		builder.clientSettings(ClientSettings.withSettings(clientSettingsMap).build());

		Map<String, Object> tokenSettingsMap = parseMap(oauth2RegisteredClient.getTokenSettings());
		builder.tokenSettings(TokenSettings.withSettings(tokenSettingsMap).build());

		return builder.build();
	}

	private Oauth2RegisteredClient toEntity(RegisteredClient registeredClient) {
		List<String> clientAuthenticationMethods = new ArrayList<>(registeredClient.getClientAuthenticationMethods().size());
		registeredClient.getClientAuthenticationMethods().forEach(clientAuthenticationMethod ->
				clientAuthenticationMethods.add(clientAuthenticationMethod.getValue()));

		List<String> authorizationGrantTypes = new ArrayList<>(registeredClient.getAuthorizationGrantTypes().size());
		registeredClient.getAuthorizationGrantTypes().forEach(authorizationGrantType ->
				authorizationGrantTypes.add(authorizationGrantType.getValue()));

		Oauth2RegisteredClient oauth2RegisteredClient = new Oauth2RegisteredClient();
		oauth2RegisteredClient.setId(registeredClient.getId());
		oauth2RegisteredClient.setClientId(registeredClient.getClientId());
		oauth2RegisteredClient.setClientIdIssuedAt(registeredClient.getClientIdIssuedAt());
		oauth2RegisteredClient.setClientSecret(registeredClient.getClientSecret());
		oauth2RegisteredClient.setClientSecretExpiresAt(registeredClient.getClientSecretExpiresAt());
		oauth2RegisteredClient.setClientName(registeredClient.getClientName());
		oauth2RegisteredClient.setClientAuthenticationMethods(StringUtils.collectionToCommaDelimitedString(clientAuthenticationMethods));
		oauth2RegisteredClient.setAuthorizationGrantTypes(StringUtils.collectionToCommaDelimitedString(authorizationGrantTypes));
		oauth2RegisteredClient.setRedirectUris(StringUtils.collectionToCommaDelimitedString(registeredClient.getRedirectUris()));
		oauth2RegisteredClient.setScopes(StringUtils.collectionToCommaDelimitedString(registeredClient.getScopes()));
		oauth2RegisteredClient.setClientSettings(writeMap(registeredClient.getClientSettings().getSettings()));
		oauth2RegisteredClient.setTokenSettings(writeMap(registeredClient.getTokenSettings().getSettings()));

		return oauth2RegisteredClient;
	}

	private Map<String, Object> parseMap(String data) {
		try {
			return this.objectMapper.readValue(data, new TypeReference<Map<String, Object>>() {});
		} catch (Exception ex) {
			throw new IllegalArgumentException(ex.getMessage(), ex);
		}
	}

	private String writeMap(Map<String, Object> data) {
		try {
			return this.objectMapper.writeValueAsString(data);
		} catch (Exception ex) {
			throw new IllegalArgumentException(ex.getMessage(), ex);
		}
	}

}
