package com.weir.sas.oauth2.mp.service.impl;

import com.weir.sas.oauth2.mp.entity.Oauth2ClientTokenSetting;
import com.weir.sas.oauth2.mp.mapper.Oauth2ClientTokenSettingMapper;
import com.weir.sas.oauth2.mp.service.IOauth2ClientTokenSettingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author weir
 * @since 2022-12-31
 */
@Service
public class Oauth2ClientTokenSettingServiceImpl extends ServiceImpl<Oauth2ClientTokenSettingMapper, Oauth2ClientTokenSetting> implements IOauth2ClientTokenSettingService {

}
