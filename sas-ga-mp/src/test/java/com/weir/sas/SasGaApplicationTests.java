package com.weir.sas;

import java.util.Collections;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

@SpringBootTest
class SasGaApplicationTests {

	@Test
	void contextLoads() {
	}
	
	public static void main(String[] args) {
		FastAutoGenerator.create("url", "username", "password")
	    .globalConfig(builder -> {
	        builder.author("baomidou") // 设置作者
	            .enableSwagger() // 开启 swagger 模式
	            .fileOverride() // 覆盖已生成文件
	            .outputDir("/Users/weir/Downloads/logs"); // 指定输出目录
	    })
	    .packageConfig(builder -> {
	        builder.parent("com.baomidou.mybatisplus.samples.generator") // 设置父包名
	            .moduleName("system") // 设置父包模块名
	            .pathInfo(Collections.singletonMap(OutputFile.xml, "/Users/weir/Downloads/logs")); // 设置mapperXml生成路径
	    })
	    .strategyConfig(builder -> {
	        builder.addInclude("t_simple") // 设置需要生成的表名
	            .addTablePrefix("t_", "c_"); // 设置过滤表前缀
	    })
	    .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
	    .execute();
	}

}
