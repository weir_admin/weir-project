package com.weir.sas;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.TemplateConfig;
import com.baomidou.mybatisplus.generator.config.builder.GeneratorBuilder;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.fill.Column;
import com.baomidou.mybatisplus.generator.fill.Property;
 
/**
 * 3.5.0
 * @author weir
 *
 */
public class CodeGenerator {
 
 
    public static void main(String[] args) {
        DataSourceConfig mysqlDataSourceConfig = new DataSourceConfig.Builder(
                "jdbc:mysql://localhost:3306/authorization_server_ga?useUnicode=true&useSSL=false&characterEncoding=utf8", "root",
                "weir336393").build();
 
//        String projectPath = System.getProperty("user.dir");
        GlobalConfig globalConfig = GeneratorBuilder.globalConfigBuilder()
//        		.openDir(true)
                .outputDir("/Users/weir/Downloads/22").author("weir").build();
 
        PackageConfig packageConfig = new PackageConfig.Builder().parent("com.weir.sas").moduleName("oauth2").build();
 
        StrategyConfig strategyConfig = new StrategyConfig.Builder().entityBuilder()// 实体配置构建者
//                .enableSerialVersionUID()
                .idType(IdType.ASSIGN_ID)
                .enableActiveRecord()
                .enableLombok()// 开启lombok模型
//                .versionColumnName("version") // 乐观锁数据库表字段
                .naming(NamingStrategy.underline_to_camel)// 数据库表映射到实体的命名策略
                .addTableFills(new Column("create_time", FieldFill.INSERT)) // 基于数据库字段填充
                .addTableFills(new Property("updateTime", FieldFill.INSERT_UPDATE)) // 基于模型属性填充
                .controllerBuilder() // 控制器属性配置构建
                .build();
 
        TemplateConfig templateConfig = new TemplateConfig.Builder().build(); // 激活所有默认模板
 
        InjectionConfig injectionConfig = new InjectionConfig.Builder().build();
        // 代码生成器
        new AutoGenerator(mysqlDataSourceConfig).global(globalConfig).packageInfo(packageConfig)
                .strategy(strategyConfig)
                .template(templateConfig)
                .injection(injectionConfig).execute();
 
    }
 
}