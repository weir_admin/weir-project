package com.weir.oauth2resource.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.weir.oauth2resource.config.JwtService;

/**
 * 资源控制器
 */
@RestController
public class ResController {
    /**
     * 受保护的资源
     * @return
     */
    @GetMapping("res")
    public String getRes() {
        return "成功获取到受保护的资源";
    }
    @GetMapping("res6")
    @PreAuthorize("hasAuthority('user_edit')")
    public String getRes6() {
    	return "hasAuthority('user_edit')+成功获取到受保护的资源";
    }
    @GetMapping("res7")
    @PreAuthorize("hasAuthority('user_add')")
    public String getRes7() {
    	return "hasAuthority('user_add')+成功获取到受保护的资源";
    }
    
    @GetMapping("/messages")
	@PreAuthorize("hasAuthority('USER') or hasAuthority('message.read') ")
	public String[] getMessages(@AuthenticationPrincipal Jwt jwt) {
		Long userId = JwtService.getUserId(jwt);
		System.out.println("------------"+userId);
		return new String[] {"Message 1", "Message 2", "Message 3"};
	}
}
