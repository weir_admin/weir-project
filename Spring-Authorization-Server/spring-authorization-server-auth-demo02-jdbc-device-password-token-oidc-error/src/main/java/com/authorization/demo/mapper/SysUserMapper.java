package com.authorization.demo.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.authorization.demo.entity.SysUserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface SysUserMapper extends BaseMapper<SysUserEntity> {

}

