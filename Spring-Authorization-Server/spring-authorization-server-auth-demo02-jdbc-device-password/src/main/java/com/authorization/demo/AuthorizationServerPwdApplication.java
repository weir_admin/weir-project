package com.authorization.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthorizationServerPwdApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthorizationServerPwdApplication.class, args);
	}

}
