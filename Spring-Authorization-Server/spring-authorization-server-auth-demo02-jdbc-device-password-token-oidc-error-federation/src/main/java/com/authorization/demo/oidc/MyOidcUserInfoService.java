package com.authorization.demo.oidc;

/**
 * @author Rommel
 * @version 1.0
 * @date 2023/7/25-16:50
 * @description TODO
 */

import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import com.authorization.demo.entity.SysUserEntity;
import com.authorization.demo.service.SysUserService;

import java.util.Map;

/**
 * Example service to perform lookup of user info for customizing an {@code id_token}.
 */
@Service
public class MyOidcUserInfoService {

    @Resource
    private SysUserService sysUserService;

    public MyOidcUserInfo loadUser(String username) {
        SysUserEntity sysUserEntity = sysUserService.selectByUsername(username);
        return new MyOidcUserInfo(this.createUser(sysUserEntity));
    }

    private Map<String, Object> createUser(SysUserEntity sysUserEntity) {
        return MyOidcUserInfo.myBuilder()
                .name(sysUserEntity.getName())
                .username(sysUserEntity.getUsername())
                .description(sysUserEntity.getDescription())
                .status(sysUserEntity.getStatus())
                .phoneNumber(sysUserEntity.getUsername())
                .email(sysUserEntity.getUsername() + "@example.com")
                .profile("https://example.com/" + sysUserEntity.getName())
                .address("XXX共和国XX省XX市XX区XXX街XXX号")
                .build()
                .getClaims();
    }

}