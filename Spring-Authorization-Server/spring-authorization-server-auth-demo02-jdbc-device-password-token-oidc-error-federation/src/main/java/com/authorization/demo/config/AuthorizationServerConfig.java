package com.authorization.demo.config;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.time.Instant;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.catalina.util.StandardSessionIdGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.oidc.IdTokenClaimNames;
import org.springframework.security.oauth2.core.oidc.OidcIdToken;
import org.springframework.security.oauth2.core.oidc.endpoint.OidcParameterNames;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.security.oauth2.jwt.JwsHeader;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtEncoder;
import org.springframework.security.oauth2.server.authorization.JdbcOAuth2AuthorizationConsentService;
import org.springframework.security.oauth2.server.authorization.JdbcOAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationConsentService;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.OAuth2TokenType;
import org.springframework.security.oauth2.server.authorization.client.JdbcRegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.config.annotation.web.configuration.OAuth2AuthorizationServerConfiguration;
import org.springframework.security.oauth2.server.authorization.config.annotation.web.configurers.OAuth2AuthorizationServerConfigurer;
import org.springframework.security.oauth2.server.authorization.settings.AuthorizationServerSettings;
import org.springframework.security.oauth2.server.authorization.token.DelegatingOAuth2TokenGenerator;
import org.springframework.security.oauth2.server.authorization.token.JwtEncodingContext;
import org.springframework.security.oauth2.server.authorization.token.JwtGenerator;
import org.springframework.security.oauth2.server.authorization.token.OAuth2AccessTokenGenerator;
import org.springframework.security.oauth2.server.authorization.token.OAuth2RefreshTokenGenerator;
import org.springframework.security.oauth2.server.authorization.token.OAuth2TokenCustomizer;
import org.springframework.security.oauth2.server.authorization.token.OAuth2TokenGenerator;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.ExceptionTranslationFilter;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.util.matcher.MediaTypeRequestMatcher;

import com.authorization.demo.device.DeviceClientAuthenticationConverter;
import com.authorization.demo.device.DeviceClientAuthenticationProvider;
import com.authorization.demo.entity.SysUserEntity;
import com.authorization.demo.federation.FederatedIdentityIdTokenCustomizer;
import com.authorization.demo.filter.MyExceptionTranslationFilter;
import com.authorization.demo.handler.MyAccessDeniedHandler;
import com.authorization.demo.handler.MyAuthenticationEntryPoint;
import com.authorization.demo.handler.MyAuthenticationFailureHandler;
import com.authorization.demo.mobile.MobileGrantAuthenticationConverter;
import com.authorization.demo.mobile.MobileGrantAuthenticationProvider;
import com.authorization.demo.oidc.MyOidcUserInfoAuthenticationConverter;
import com.authorization.demo.oidc.MyOidcUserInfoAuthenticationProvider;
import com.authorization.demo.oidc.MyOidcUserInfoService;
import com.authorization.demo.pwd.PasswordGrantAuthenticationConverter;
import com.authorization.demo.pwd.PasswordGrantAuthenticationProvider;
import com.authorization.demo.service.SysUserService;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.source.ImmutableJWKSet;
import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.proc.SecurityContext;

import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class AuthorizationServerConfig {

	private static final String CUSTOM_CONSENT_PAGE_URI = "/oauth2/consent";
	@Resource
	private SysUserService sysUserService;
	@Resource
	UserDetailsService userDetailsService;
	@Resource
	private MyOidcUserInfoService myOidcUserInfoService;

	/**
	 * Spring Authorization Server 相关配置
	 * 此处方法与下面defaultSecurityFilterChain都是SecurityFilterChain配置，配置的内容有点区别， 因为Spring
	 * Authorization Server是建立在Spring Security 基础上的，defaultSecurityFilterChain方法主要
	 * 配置Spring Security相关的东西，而此处authorizationServerSecurityFilterChain方法主要配置OAuth
	 * 2.1和OpenID Connect 1.0相关的东西
	 */
	@Bean
	@Order(1)
	public SecurityFilterChain authorizationServerSecurityFilterChain(HttpSecurity http,
			RegisteredClientRepository registeredClientRepository,
			AuthorizationServerSettings authorizationServerSettings, OAuth2AuthorizationService authorizationService,
			OAuth2TokenGenerator<?> tokenGenerator) throws Exception {
		OAuth2AuthorizationServerConfiguration.applyDefaultSecurity(http);

		// AuthenticationConverter(预处理器)，尝试从HttpServletRequest提取客户端凭据,用以构建OAuth2ClientAuthenticationToken实例。
		DeviceClientAuthenticationConverter deviceClientAuthenticationConverter = new DeviceClientAuthenticationConverter(
				authorizationServerSettings.getDeviceAuthorizationEndpoint());
		// AuthenticationProvider(主处理器)，用于验证OAuth2ClientAuthenticationToken。
		DeviceClientAuthenticationProvider deviceClientAuthenticationProvider = new DeviceClientAuthenticationProvider(
				registeredClientRepository);

		http.getConfigurer(OAuth2AuthorizationServerConfigurer.class)
				.deviceAuthorizationEndpoint(deviceAuthorizationEndpoint ->
				// 设置用户码校验地址
				deviceAuthorizationEndpoint.verificationUri("/activate"))
				.deviceVerificationEndpoint(deviceVerificationEndpoint ->
				// 设置授权页地址
				deviceVerificationEndpoint.consentPage(CUSTOM_CONSENT_PAGE_URI))
				.clientAuthentication(clientAuthentication ->
				// 设置AuthenticationConverter(预处理器)和AuthenticationProvider(主处理器)
				clientAuthentication.authenticationConverter(deviceClientAuthenticationConverter)
						.authenticationProvider(deviceClientAuthenticationProvider)
						.errorResponseHandler(new MyAuthenticationFailureHandler()))
				.authorizationEndpoint(
						authorizationEndpoint -> authorizationEndpoint.consentPage(CUSTOM_CONSENT_PAGE_URI))
				// 设置自定义密码模式
				.tokenEndpoint(tokenEndpoint -> tokenEndpoint
						.accessTokenRequestConverter(new PasswordGrantAuthenticationConverter()).authenticationProvider(
								new PasswordGrantAuthenticationProvider(authorizationService, tokenGenerator)))
				// 设置自定义手机验证码模式
				.tokenEndpoint(tokenEndpoint -> tokenEndpoint
						.accessTokenRequestConverter(new MobileGrantAuthenticationConverter())
						.authenticationProvider(
								new MobileGrantAuthenticationProvider(authorizationService, tokenGenerator))
						.errorResponseHandler(new MyAuthenticationFailureHandler()))
				// 开启OpenID Connect 1.0（其中oidc为OpenID Connect的缩写）。
				.oidc(
//                		Customizer.withDefaults()
						oidcCustomizer -> {
							oidcCustomizer.userInfoEndpoint(userInfoEndpointCustomizer -> {
								userInfoEndpointCustomizer.userInfoRequestConverter(
										new MyOidcUserInfoAuthenticationConverter(myOidcUserInfoService));
								userInfoEndpointCustomizer.authenticationProvider(
										new MyOidcUserInfoAuthenticationProvider(authorizationService));
							});
						});

		// 设置登录地址，需要进行认证的请求被重定向到该地址
		http.addFilterBefore(new MyExceptionTranslationFilter(), ExceptionTranslationFilter.class)
				.exceptionHandling((exceptions) -> exceptions
						.defaultAuthenticationEntryPointFor(new LoginUrlAuthenticationEntryPoint("/login"),
								new MediaTypeRequestMatcher(MediaType.TEXT_HTML))
						.authenticationEntryPoint(new MyAuthenticationEntryPoint())
						.accessDeniedHandler(new MyAccessDeniedHandler()))
				.oauth2ResourceServer(oauth2ResourceServer -> oauth2ResourceServer.jwt(Customizer.withDefaults()));

		return http.build();
	}

	/**
	 * 客户端信息 对应表：oauth2_registered_client
	 */
	@Bean
	public RegisteredClientRepository registeredClientRepository(JdbcTemplate jdbcTemplate) {
		return new JdbcRegisteredClientRepository(jdbcTemplate);
	}

	/**
	 * 授权信息 对应表：oauth2_authorization
	 */
	@Bean
	public OAuth2AuthorizationService authorizationService(JdbcTemplate jdbcTemplate,
			RegisteredClientRepository registeredClientRepository) {
		return new JdbcOAuth2AuthorizationService(jdbcTemplate, registeredClientRepository);
	}

	/**
	 * 授权确认 对应表：oauth2_authorization_consent
	 */
	@Bean
	public OAuth2AuthorizationConsentService authorizationConsentService(JdbcTemplate jdbcTemplate,
			RegisteredClientRepository registeredClientRepository) {
		return new JdbcOAuth2AuthorizationConsentService(jdbcTemplate, registeredClientRepository);
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	/**
	 * 配置 JWK，为JWT(id_token)提供加密密钥，用于加密/解密或签名/验签
	 * JWK详细见：https://datatracker.ietf.org/doc/html/draft-ietf-jose-json-web-key-41
	 */
	@Bean
	public JWKSource<SecurityContext> jwkSource() {
		KeyPair keyPair = generateRsaKey();
		RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
		RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
		RSAKey rsaKey = new RSAKey.Builder(publicKey).privateKey(privateKey).keyID(UUID.randomUUID().toString())
				.build();
		JWKSet jwkSet = new JWKSet(rsaKey);
		return new ImmutableJWKSet<>(jwkSet);
	}

	/**
	 * 生成RSA密钥对，给上面jwkSource() 方法的提供密钥对
	 */
	private static KeyPair generateRsaKey() {
		KeyPair keyPair;
		try {
			KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
			keyPairGenerator.initialize(2048);
			keyPair = keyPairGenerator.generateKeyPair();
		} catch (Exception ex) {
			throw new IllegalStateException(ex);
		}
		return keyPair;
	}

	/**
	 * 配置jwt解析器
	 */
	@Bean
	public JwtDecoder jwtDecoder(JWKSource<SecurityContext> jwkSource) {
		return OAuth2AuthorizationServerConfiguration.jwtDecoder(jwkSource);
	}

	/**
	 * 配置认证服务器请求地址
	 */
	@Bean
	public AuthorizationServerSettings authorizationServerSettings() {
		// 什么都不配置，则使用默认地址
		return AuthorizationServerSettings.builder().build();
	}

	/**
	 * 配置token生成器
	 */
	@Bean
	OAuth2TokenGenerator<?> tokenGenerator(JWKSource<SecurityContext> jwkSource) {
		JwtGenerator jwtGenerator = new JwtGenerator(new NimbusJwtEncoder(jwkSource));
		jwtGenerator.setJwtCustomizer(jwtCustomizer());
		OAuth2AccessTokenGenerator accessTokenGenerator = new OAuth2AccessTokenGenerator();
		OAuth2RefreshTokenGenerator refreshTokenGenerator = new OAuth2RefreshTokenGenerator();
		return new DelegatingOAuth2TokenGenerator(jwtGenerator, accessTokenGenerator, refreshTokenGenerator);
	}

	@Bean
	public OAuth2TokenCustomizer<JwtEncodingContext> jwtCustomizer() {

		return context -> {
			JwsHeader.Builder headers = context.getJwsHeader();
			JwtClaimsSet.Builder claims = context.getClaims();
			SysUserEntity sysUserEntity = sysUserService.selectByUsername(context.getPrincipal().getName());
			if (context.getTokenType().equals(OAuth2TokenType.ACCESS_TOKEN)) {
				// Customize headers/claims for access_token
				claims.claims(claimsConsumer -> {
					UserDetails userDetails = userDetailsService.loadUserByUsername(context.getPrincipal().getName());
					claimsConsumer.merge("scope", userDetails.getAuthorities(), (scope, authorities) -> {
						Set<String> scopeSet = (Set<String>) scope;
						Collection<SimpleGrantedAuthority> simpleGrantedAuthorities = (Collection<SimpleGrantedAuthority>) authorities;
						simpleGrantedAuthorities.stream().forEach(simpleGrantedAuthority -> {
							if (!scopeSet.contains(simpleGrantedAuthority.getAuthority())) {
								scopeSet.add(simpleGrantedAuthority.getAuthority());
							}
						});
						return scopeSet;
					});
				});

			} else if (context.getTokenType().getValue().equals(OidcParameterNames.ID_TOKEN)) {
				// Customize headers/claims for id_token
				claims.claim(IdTokenClaimNames.AUTH_TIME, Date.from(Instant.now()));
				StandardSessionIdGenerator standardSessionIdGenerator = new StandardSessionIdGenerator();
				claims.claim("sid", standardSessionIdGenerator.generateSessionId());
				claims.claim("username", sysUserEntity.getUsername());
				claims.claim("name", sysUserEntity.getName());
				claims.claim("description", sysUserEntity.getDescription());

				Map<String, Object> thirdPartyClaims = extractClaims(context.getPrincipal());
				context.getClaims().claims(existingClaims -> {
					// Remove conflicting claims set by this authorization server
					existingClaims.keySet().forEach(thirdPartyClaims::remove);

					// Remove standard id_token claims that could cause problems with clients
					ID_TOKEN_CLAIMS.forEach(thirdPartyClaims::remove);

					// Add all other claims directly to id_token
					existingClaims.putAll(thirdPartyClaims);
				});
			}
			// 检查登录用户信息是不是OAuth2User，在token中添加loginType属性
			if (context.getPrincipal().getPrincipal() instanceof OAuth2User user) {
//				JwtClaimsSet.Builder claims = context.getClaims();
				Object loginType = user.getAttribute("loginType");
				if (loginType instanceof String) {
					// 同时检验是否为String和是否不为空
					claims.claim("loginType", loginType);
				}
			}

			// 检查登录用户信息是不是UserDetails，排除掉没有用户参与的流程
			if (context.getPrincipal().getPrincipal() instanceof UserDetails user) {
				// 获取申请的scopes
				Set<String> scopes = context.getAuthorizedScopes();
				// 获取用户的权限
				Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
				// 提取权限并转为字符串
				Set<String> authoritySet = Optional.ofNullable(authorities).orElse(Collections.emptyList()).stream()
						// 获取权限字符串
						.map(GrantedAuthority::getAuthority)
						// 去重
						.collect(Collectors.toSet());

				// 合并scope与用户信息
				authoritySet.addAll(scopes);

//				JwtClaimsSet.Builder claims = context.getClaims();
				// 将权限信息放入jwt的claims中（也可以生成一个以指定字符分割的字符串放入）
				claims.claim("authorities", authoritySet);
				// 放入其它自定内容
				// 角色、头像...
			}
		};
	}

	private static final Set<String> ID_TOKEN_CLAIMS = Set.of(IdTokenClaimNames.ISS, IdTokenClaimNames.SUB,
			IdTokenClaimNames.AUD, IdTokenClaimNames.EXP, IdTokenClaimNames.IAT, IdTokenClaimNames.AUTH_TIME,
			IdTokenClaimNames.NONCE, IdTokenClaimNames.ACR, IdTokenClaimNames.AMR, IdTokenClaimNames.AZP,
			IdTokenClaimNames.AT_HASH, IdTokenClaimNames.C_HASH);

	private Map<String, Object> extractClaims(Authentication principal) {
		Map<String, Object> claims;
		if (principal.getPrincipal() instanceof OidcUser oidcUser) {
			OidcIdToken idToken = oidcUser.getIdToken();
			claims = idToken.getClaims();
		} else if (principal.getPrincipal() instanceof OAuth2User oauth2User) {
			claims = oauth2User.getAttributes();
		} else {
			claims = Collections.emptyMap();
		}

		return new HashMap<>(claims);
	}

//    @Bean
//    public OAuth2TokenCustomizer<JwtEncodingContext> oAuth2TokenCustomizer() {
//        return new FederatedIdentityIdTokenCustomizer();
//    }
}