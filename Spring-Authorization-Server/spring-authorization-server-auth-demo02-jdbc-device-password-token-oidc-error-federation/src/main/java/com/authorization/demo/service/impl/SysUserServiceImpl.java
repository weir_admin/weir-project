package com.authorization.demo.service.impl;

import org.springframework.stereotype.Service;

import com.authorization.demo.entity.Oauth2ThirdAccount;
import com.authorization.demo.entity.SysUserEntity;
import com.authorization.demo.mapper.SysUserMapper;
import com.authorization.demo.service.SysUserService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUserEntity> implements SysUserService {
    @Override
    public SysUserEntity selectByUsername(String username) {
        LambdaQueryWrapper<SysUserEntity> lambdaQueryWrapper = new LambdaQueryWrapper();
        lambdaQueryWrapper.eq(SysUserEntity::getUsername,username);
        return this.getOne(lambdaQueryWrapper);
    }
    
    @Override
	public Integer saveByThirdAccount(Oauth2ThirdAccount thirdAccount) {
		SysUserEntity user = new SysUserEntity();
		user.setName(thirdAccount.getName());
		user.setStatus(1);
		user.setUsername(thirdAccount.getThirdUsername());
		user.setAvatarUrl(thirdAccount.getAvatarUrl());
		user.setSourceFrom(thirdAccount.getType());
		this.save(user);
		return user.getId();
	}
}