package com.authorization.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthorizationServerOIDCApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthorizationServerOIDCApplication.class, args);
	}

}
