/*
 * Copyright 2020-2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.authorization.demo;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.ShearCaptcha;
import jakarta.servlet.http.HttpSession;

/**
 * @author Steve Riesenberg
 * @since 1.1
 */
@Controller
public class LoginController {

	@GetMapping("/login")
	public String login(Model model, HttpSession session) {
		Object attribute = session.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
	    if (attribute instanceof AuthenticationException exception) {
	        model.addAttribute("error", exception.getMessage());
	    }
	    return "login";
	}
//	@GetMapping("/login")
//	public String login() {
//		return "login";
//	}
	
	@ResponseBody
	@GetMapping("/getCaptcha")
	public Map<String,Object> getCaptcha(HttpSession session) {
	    // 使用hutool-captcha生成图形验证码
	    // 定义图形验证码的长、宽、验证码字符数、干扰线宽度
	    ShearCaptcha captcha = CaptchaUtil.createShearCaptcha(150, 40, 4, 2);
	    // 这里应该返回一个统一响应类，暂时使用map代替
	    Map<String,Object> result = new HashMap<>();
	    result.put("code", HttpStatus.OK.value());
	    result.put("success", true);
	    result.put("message", "获取验证码成功.");
	    result.put("data", captcha.getImageBase64Data());
	    // 存入session中
	    session.setAttribute("captcha", captcha.getCode());
	    return result;
	}


}