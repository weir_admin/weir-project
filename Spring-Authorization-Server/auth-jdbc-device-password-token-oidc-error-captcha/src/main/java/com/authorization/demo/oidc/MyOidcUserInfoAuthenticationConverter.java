package com.authorization.demo.oidc;

import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.lang.Nullable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.security.oauth2.server.authorization.oidc.authentication.OidcUserInfoAuthenticationToken;
import org.springframework.security.web.authentication.AuthenticationConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Rommel
 * @version 1.0
 * @date 2023/7/20-14:37
 * @description TODO
 */
public class MyOidcUserInfoAuthenticationConverter implements AuthenticationConverter {

    private MyOidcUserInfoService myOidcUserInfoService;


    public MyOidcUserInfoAuthenticationConverter(MyOidcUserInfoService myOidcUserInfoService){
        this.myOidcUserInfoService = myOidcUserInfoService;
    }

    @Nullable
    @Override
    public Authentication convert(HttpServletRequest request) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        //查询用户信息
        MyOidcUserInfo myOidcUserInfo = myOidcUserInfoService.loadUser(authentication.getName());

        //返回自定义的OidcUserInfoAuthenticationToken
        return new OidcUserInfoAuthenticationToken(authentication, myOidcUserInfo);
    }
    
}