package com.authorization.demo.mobile;

import java.util.Map;

import org.springframework.lang.Nullable;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.server.authorization.authentication.OAuth2AuthorizationGrantAuthenticationToken;

import com.authorization.demo.constant.OAuth2Constant;

public class MobileGrantAuthenticationToken extends OAuth2AuthorizationGrantAuthenticationToken {

    public MobileGrantAuthenticationToken(Authentication clientPrincipal,
                                          @Nullable Map<String, Object> additionalParameters) {
        super(new AuthorizationGrantType(OAuth2Constant.GRANT_TYPE_MOBILE),
                clientPrincipal, additionalParameters);
    }

}