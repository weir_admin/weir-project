package com.weir.resource;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/resource")
public class HelloController {

	@GetMapping("/get")
	private String get() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		System.out.println("------------------------"+authentication);
		return "resource-get";
	}
	@PreAuthorize("hasAuthority('resource-get')")
	@GetMapping("/get2")
	private String get2() {
		return "resource-get2";
	}
	@PreAuthorize("hasAnyRole('ROLE_USER')")
	@GetMapping("/get3")
	private String get3() {
		return "resource-get3";
	}
	
	@GetMapping("res6")
    @PreAuthorize("hasAuthority('user_edit')")
    public String getRes6() {
    	return "hasAuthority('user_edit')+成功获取到受保护的资源";
    }
    @GetMapping("res7")
    @PreAuthorize("hasAuthority('user_add')")
    public String getRes7() {
    	return "hasAuthority('user_add')+成功获取到受保护的资源";
    }
}
