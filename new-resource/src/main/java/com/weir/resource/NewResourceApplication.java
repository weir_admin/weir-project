package com.weir.resource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

//@EnableDiscoveryClient
@SpringBootApplication
public class NewResourceApplication {

	public static void main(String[] args) {
		SpringApplication.run(NewResourceApplication.class, args);
	}

}
