//package com.weir.resource.config;
//package com.weir.resource.config;
//
//import java.io.IOException;
//import java.nio.file.Files;
//import java.security.KeyFactory;
//import java.security.NoSuchAlgorithmException;
//import java.security.interfaces.RSAPublicKey;
//import java.security.spec.InvalidKeySpecException;
//import java.security.spec.X509EncodedKeySpec;
//import java.util.Base64;
//import java.util.Collections;
//
//import org.springframework.boot.web.client.RestTemplateBuilder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.io.FileSystemResource;
//import org.springframework.core.io.Resource;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;
//import org.springframework.security.config.http.SessionCreationPolicy;
//import org.springframework.security.oauth2.jwt.JwtDecoder;
//import org.springframework.security.oauth2.jwt.JwtValidators;
//import org.springframework.security.oauth2.jwt.MappedJwtClaimSetConverter;
//import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
//import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
//import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;
//import org.springframework.security.web.SecurityFilterChain;
//import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
//
//
//@Configuration
//@EnableWebSecurity
//@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
//public class ResourceServerConfig {
//
//	@Bean
//	SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
//		http
//		.csrf().disable()
////		.httpBasic().disable()
//		
////			.mvcMatcher("/resource/**")
//				.authorizeRequests((authorize) -> authorize
//						.anyRequest().authenticated())
////					.mvcMatchers("/resource/**")
////					.access("hasAuthority('SCOPE_message.read')")
////					.and()
//				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
//				.and()
////				.addFilterAt(new JWTAuthenticationFilter(jwtAuthenticationManager),
////						UsernamePasswordAuthenticationFilter.class)
//			.oauth2ResourceServer()
//				.jwt()
//			.decoder(jwtDecoder())
//			;
//		return http.build();
//	}
//
//	@Bean
//    public JwtAuthenticationConverter jwtAuthenticationConverter() {
//		System.out.println("------------jwtAuthenticationConverter--------");
//        JwtGrantedAuthoritiesConverter grantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
//        grantedAuthoritiesConverter.setAuthoritiesClaimName("authorities");
//        grantedAuthoritiesConverter.setAuthorityPrefix("");
//
//        JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
//        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(grantedAuthoritiesConverter);
//        return jwtAuthenticationConverter;
//    }
//	
//	/**
//     * jwt 的解码器
//     *
//     * @return JwtDecoder
//     */
//    public JwtDecoder jwtDecoder() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
//        // 方式一：通过 jwks url 构建
//        // NimbusJwtDecoder.JwkSetUriJwtDecoderBuilder jwtDecoderBuilder = NimbusJwtDecoder.withJwkSetUri("http://qq.com:8080/oauth2/jwks");
//
//        // 方式二：通过收取服务器的公钥创建
//        Resource resource = new FileSystemResource("/Users/weir/git/weir-project/new-resource/public-key.pem");
//        String publicKeyStr = String.join("", Files.readAllLines(resource.getFile().toPath()));
//        byte[] publicKeyBytes = Base64.getDecoder().decode(publicKeyStr);
//        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicKeyBytes);
//        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
//        RSAPublicKey rsaPublicKey = (RSAPublicKey)keyFactory.generatePublic(keySpec);
//        NimbusJwtDecoder.PublicKeyJwtDecoderBuilder jwtDecoderBuilder = NimbusJwtDecoder.withPublicKey(rsaPublicKey);
//
//        // 授权服务器 jwk 的信息
//        NimbusJwtDecoder decoder = jwtDecoderBuilder
//                // 设置获取 jwk 信息的超时时间
//                // .restOperations(
//                //         builder.setReadTimeout(Duration.ofSeconds(3))
//                //                 .setConnectTimeout(Duration.ofSeconds(3))
//                //                 .build()
//                // )
//                .build();
//        // 对jwt进行校验
//        decoder.setJwtValidator(JwtValidators.createDefault());
//        // 对 jwt 的 claim 中增加值
//        decoder.setClaimSetConverter(
//                MappedJwtClaimSetConverter.withDefaults(Collections.singletonMap("为claim中增加key", custom -> "值"))
//        );
//        return decoder;
//    }
//}
