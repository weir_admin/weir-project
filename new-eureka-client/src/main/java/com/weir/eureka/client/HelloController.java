package com.weir.eureka.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.weir.user.entity.PermUserDTO;
import com.weir.user.service.UserServiceFeign;

/**
 * 展示eureka客户端通信
 */
@RestController
@RequestMapping("/hello")
public class HelloController {

	@Autowired
	private UserServiceFeign userServiceFeign;
	
	@GetMapping("/get")
    public PermUserDTO name() {
		return userServiceFeign.get(1);
//		return "weir";
	}
}
