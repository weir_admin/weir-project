//package com.weir.user;
//
//import java.util.Arrays;
//
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import com.weir.user.entity.PermMenu;
//import com.weir.user.entity.PermRole;
//import com.weir.user.entity.PermUser;
//import com.weir.user.repository.RoleRepository;
//import com.weir.user.repository.UserRepository;
//
//@SpringBootTest
//class NewUserRBACApplicationTests {
//
//	@Autowired
//	private UserRepository userRepository;
//	@Autowired
//	private RoleRepository roleRepository;
//	@Test
//	void contextLoads() {
//		System.out.println("----------11------------------");
//		PermUser u = new PermUser();
//		u.setPwd("123456");
//		u.setUsername("weir");
//		
//		PermRole r = new PermRole();
//		r.setCode("admin");
//		r.setName("管理员");
//		
//		u.setRoles(Arrays.asList(r));
//		
////		PermMenu m = new PermMenu();
////		m.setCode("user_add");
////		m.setStep(2);
////		m.setText("用户添加");
////		PermMenu m2 = new PermMenu();
////		m2.setCode("user_edit");
////		m2.setStep(2);
////		m2.setText("用户修改");
////		
////		r.setPermissions(Arrays.asList(m,m2));
//		
//		userRepository.save(u);
//		
//		System.out.println("-------------ok----------------");
//	}
//	
//	@Test
//	void contextLoadsRole() {
//		System.out.println("----------11------------------");
//		
//		PermRole r = new PermRole();
//		r.setCode("admin_weir");
//		r.setName("管理员weir");
//		
//		PermMenu m = new PermMenu();
//		m.setCode("user_add");
//		m.setStep(2);
//		m.setText("用户添加");
//		PermMenu m2 = new PermMenu();
//		m2.setCode("user_edit");
//		m2.setStep(2);
//		m2.setText("用户修改");
//		
//		r.setPermissions(Arrays.asList(m,m2));
//		
//		roleRepository.save(r);
//		
//		System.out.println("-------------ok----------------");
//	}
//	@Test
//	void getuser() {
//		System.out.println("----------11------------------");
//		
//		PermUser permUser = userRepository.getReferenceById(1);
//		
//		System.out.println("-------------ok----------------"+ permUser.toString());
//	}
//
//}
