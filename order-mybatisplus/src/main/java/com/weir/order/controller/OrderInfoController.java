package com.weir.order.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.weir.order.entity.OrderInfo;
import com.weir.order.entity.OrderItem;
import com.weir.order.service.IOrderInfoService;
import com.weir.order.service.IOrderItemService;
import com.weir.order.service.impl.IdGenerator;
import com.weir.order.utils.ChineseName;
import com.weir.order.utils.OrderNoCreate;
import com.weir.order.utils.RandomPhoneNumGenerator;
import com.weir.order.utils.SFExpressNumberGenerator;
import com.weir.order.utils.Sequence;

@RestController
@RequestMapping("/order")
public class OrderInfoController {

	@Autowired
	private IOrderInfoService orderInfoService;
	@Autowired
	private IOrderItemService orderItemService;

	private final Sequence sequence = new Sequence(null);

	@GetMapping("/list-page")
	public Page<OrderInfo> listPage(@RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
		Page<OrderInfo> page = orderInfoService.page(new Page<OrderInfo>(pageNo, pageSize));
		return page;
	}

	@GetMapping("add-batch")
	public List<OrderInfo> addBatch() {
		long a = System.currentTimeMillis();
		List<OrderInfo> orderInfos = new ArrayList<>();
		List<OrderItem> orderItems = new ArrayList<>();
		for (int i = 0; i < 100; i++) {
			OrderInfo orderInfo = new OrderInfo();
			orderInfo.setId(sequence.nextId());
			orderInfo.setClientType("PC");
			orderInfo.setConsigneeAddressPath("龙城镇钮王村");
			orderInfo.setConsigneeDetail("龙城镇钮王村");
			orderInfo.setConsigneeMobile(RandomPhoneNumGenerator.generatePhoneNum());
			orderInfo.setConsigneeName(ChineseName.getName());
			orderInfo.setDeliverStatus("UNDELIVERED");
			orderInfo.setDeliveryMethod("LOGISTICS");
			orderInfo.setDiscountPrice(30d);
			orderInfo.setFreightPrice(600d);
			orderInfo.setGoodsNum(2);
			orderInfo.setGoodsPrice(21.3);
			orderInfo.setLogisticsCode("SF");
			orderInfo.setLogisticsName("顺丰快递");
			orderInfo.setLogisticsNo(SFExpressNumberGenerator.generateSFExpressNumber());
			orderInfo.setLogisticsTime(new Date());
			orderInfo.setMemberId(IdWorker.getIdStr());
			orderInfo.setMemberName(ChineseName.getName());
			orderInfo.setNeedReceipt(false);
			orderInfo.setOrderCount(4);
			orderInfo.setOrderMoney(45.5);
			orderInfo.setOrderNo(OrderNoCreate.randomOrderCode());
			orderInfo.setOrderPromotionType("NORMAL");
			orderInfo.setOrderStatus("UNDELIVERED");
			orderInfo.setOrderType("NORMAL");
			orderInfo.setPaymentMethod("WECHAT");
			orderInfo.setPaymentTime(new Date());
			orderInfo.setPayStatus("UNPAID");
			orderInfo.setStoreName("路口商店");
			orderInfos.add(orderInfo);
			for (int j = 0; j < 3; j++) {
				OrderItem orderItem = new OrderItem();
				orderItem.setId(sequence.nextId());
				orderItem.setAfterSaleStatus("NOT_APPLIED");
				orderItem.setCategoryId(Integer.valueOf(i + 1).toString());
				orderItem.setCreateTime(new Date());
				orderItem.setCreator(i + 1);
				orderItem.setFlowPrice(44.5);
				orderItem.setGoodId(i + 1);
				orderItem.setGoodMoney(33.5);
				orderItem.setGoodsName("冰城雪梨");
				orderItem.setGoodsPrice(4.5);
				orderItem.setImage("xxx/xxxxxxx/xxxxxxx.jpg");
				orderItem.setMemberId(i + 1);
				orderItem.setNum(2);
				orderItem.setOrderId(orderInfo.getId());
				orderItem.setOrderItemSn(orderInfo.getOrderNo() + j + 1);
				orderItems.add(orderItem);
			}
		}
		
		List<OrderInfo> orderInfoList = orderInfoList(orderInfos);
		List<OrderItem> orderItemList = orderItemList(orderItems);
//		orderItems = orderItemList(orderItems);
		System.out.println("------------------------------------:"+orderInfoList);
		orderInfoService.insertBatch(orderInfoList, 50);
		orderItemService.insertBatch(orderItemList, 50);
		long b = System.currentTimeMillis();
		return orderInfoList;
//		return "耗时：" + (b - a);
	}
	
	private List<OrderInfo> orderInfoList(List<OrderInfo> list) {
		List<Long> ls = new ArrayList<>();
		List<OrderInfo> ois = new ArrayList<>();
		for (OrderInfo orderInfo : list) {
			if (!ls.contains(orderInfo.getId())) {
				ls.add(orderInfo.getId());
				ois.add(orderInfo);
			}
		}
		return ois;
	}
	private List<OrderItem> orderItemList(List<OrderItem> list) {
		List<Long> ls = new ArrayList<>();
		List<OrderItem> ois = new ArrayList<>();
		for (OrderItem orderInfo : list) {
			if (!ls.contains(orderInfo.getId())) {
				ls.add(orderInfo.getId());
				ois.add(orderInfo);
			}
		}
		return ois;
	}

	static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
		Map<Object, Boolean> seen = new ConcurrentHashMap<>();
		// putIfAbsent方法添加键值对，如果map集合中没有该key对应的值，则直接添加，并返回null，如果已经存在对应的值，则依旧为原来的值。
		// 如果返回null表示添加数据成功(不重复)，不重复(null==null :TRUE)
		return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
	}

	@GetMapping("add-batch-test")
	public String addBatchTest() {
		long a = System.currentTimeMillis();
		List<Long> orderInfos = new ArrayList<>();
		for (int i = 0; i < 10000; i++) {
			orderInfos.add(IdWorker.getId());
		}
		Map<Long, Long> map = orderInfos.stream()
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
		List<Long> collect = map.entrySet().stream().filter(e -> e.getValue() > 1).map(Map.Entry::getKey)
				.collect(Collectors.toList());

		long b = System.currentTimeMillis();
		return "耗时：" + (b - a) + collect;
	}
}
