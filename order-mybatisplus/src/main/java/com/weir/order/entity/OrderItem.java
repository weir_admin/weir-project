package com.weir.order.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;


/**
 * 订单子表
 * @author weir
 *
 */
@Data
@TableName("order_item")
public class OrderItem {
	
	private static final long serialVersionUID = -7813409518159851507L;

	@TableId(value = "id", type = IdType.INPUT)
	private Long id;
	private Long orderId;

	private String orderNo;

    private Integer memberId;

    private Integer goodId;

    private Double goodMoney;
	
//    @ApiModelProperty(value = "子订单编号")
    private String orderItemSn;

//    @ApiModelProperty(value = "单价")
    private Double unitPrice;

//    @ApiModelProperty(value = "小记")
    private Double subTotal;

//    @ApiModelProperty(value = "商品ID")
    private String goodsId;

//    @ApiModelProperty(value = "货品ID")
    private String skuId;

//    @ApiModelProperty(value = "销售量")
    private Integer num;

//    @ApiModelProperty(value = "交易编号")
    private String tradeSn;

//    @ApiModelProperty(value = "图片")
    private String image;

//    @ApiModelProperty(value = "商品名称")
    private String goodsName;

//    @ApiModelProperty(value = "分类ID")
    private String categoryId;

//    @ApiModelProperty(value = "快照id")
    private String snapshotId;

//    @ApiModelProperty(value = "规格json")
    private String specs;

//    @ApiModelProperty(value = "促销类型")
    private String promotionType;

//    @ApiModelProperty(value = "促销id")
    private String promotionId;

//    @ApiModelProperty(value = "销售金额")
    private Double goodsPrice;

//    @ApiModelProperty(value = "实际金额")
    private Double flowPrice;

    /**
     * @see CommentStatusEnum
     */
//    @ApiModelProperty(value = "评论状态:未评论(UNFINISHED),待追评(WAIT_CHASE),评论完成(FINISHED)，")
    private String commentStatus;

    /**
     * @see OrderItemAfterSaleStatusEnum
     */
//    @ApiModelProperty(value = "售后状态")
    private String afterSaleStatus;

//    @ApiModelProperty(value = "价格详情")
    private String priceDetail;

    /**
     * @see OrderComplaintStatusEnum
     */
//    @ApiModelProperty(value = "投诉状态")
    private String complainStatus;

//    @ApiModelProperty(value = "交易投诉id")
    private String complainId;

//    @ApiModelProperty(value = "退货商品数量")
    private Integer returnGoodsNumber;
    private Integer creator;
    private Date createTime;

}
