package com.weir.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.weir.order.entity.OrderInfo;

public interface OrderInfoMapper extends MyBaseMapper<OrderInfo> {
}
