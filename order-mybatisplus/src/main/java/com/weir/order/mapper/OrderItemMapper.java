package com.weir.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.weir.order.entity.OrderItem;

public interface OrderItemMapper extends MyBaseMapper<OrderItem> {
}
