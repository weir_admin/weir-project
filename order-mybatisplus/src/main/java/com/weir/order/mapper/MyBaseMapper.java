package com.weir.order.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface MyBaseMapper<T> extends BaseMapper<T> {
    
    

    int insertBatchSomeColumn(@Param("list") List<T> batchList);

}