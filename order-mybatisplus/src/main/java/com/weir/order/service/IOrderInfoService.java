package com.weir.order.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.weir.order.entity.OrderInfo;

public interface IOrderInfoService extends IService<OrderInfo>{

	int insertBatch(List<OrderInfo> list, int batchSize);

}
