package com.weir.order.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.weir.order.entity.OrderInfo;
import com.weir.order.entity.OrderItem;

public interface IOrderItemService extends IService<OrderItem>{

	int insertBatch(List<OrderItem> list, int batchSize);

}
