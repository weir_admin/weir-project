package com.weir.order.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.weir.order.entity.OrderInfo;
import com.weir.order.mapper.OrderInfoMapper;
import com.weir.order.service.IOrderInfoService;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 菜单 服务实现类
 * </p>
 *
 * @author weir
 * @since 2018-12-24
 */
@Service
public class OrderInfoServiceImpl extends ServiceImpl<OrderInfoMapper, OrderInfo> implements IOrderInfoService {
	
	@Autowired
	private OrderInfoMapper orderInfoMapper;
	
	@Override
	@Transactional(rollbackFor = {Exception.class})
	public int insertBatch(List<OrderInfo> list, int batchSize) {
		int size = list.size();
		int min = Math.min(batchSize, size);
		int i = 1;
		List<OrderInfo> oneList = new ArrayList();
		for (int j = 0; j < size; j++) {
			oneList.add(list.get(j));
			i++;
			if (i == min) {
				orderInfoMapper.insertBatchSomeColumn(list);
				oneList.clear();
				min = Math.min(min+batchSize, size);
			}
		}
		if (!oneList.isEmpty()) {
			orderInfoMapper.insertBatchSomeColumn(list);
		}
		return size;
	}
}
