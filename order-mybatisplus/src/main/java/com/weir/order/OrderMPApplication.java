package com.weir.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@SpringBootApplication
public class OrderMPApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderMPApplication.class, args);
	}

}
