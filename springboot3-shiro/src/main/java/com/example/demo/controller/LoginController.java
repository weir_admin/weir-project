package com.example.demo.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.demo.common.constant.CommonConstant;
import com.example.demo.common.system.vo.LoginUser;
import com.example.demo.common.system.vo.Result;
import com.example.demo.system.entity.SysUser;
import com.example.demo.system.model.SysLoginModel;
import com.example.demo.system.serivce.ISysUserService;
import com.example.demo.utils.JwtUtil;
import com.example.demo.utils.MD5Util;
import com.example.demo.utils.PasswordUtil;
import com.example.demo.utils.RedisUtil;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * @Author scott
 * @since 2018-12-17
 */
@RestController
@RequestMapping("/sys")
@Slf4j
public class LoginController {
	@Autowired
	private ISysUserService sysUserService;
	@Autowired
	private RedisUtil redisUtil;

//	@Lazy
//	@Resource
//	private CommonAPI commonAPI;

	private static final String BASE_CHECK_CODES = "qwertyuiplkjhgfdsazxcvbnmQWERTYUPLKJHGFDSAZXCVBNM1234567890";

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public Result<JSONObject> login(@RequestBody SysLoginModel sysLoginModel) {
		Result<JSONObject> result = new Result<JSONObject>();
		String username = sysLoginModel.getUsername();
		String password = sysLoginModel.getPassword();
		// update-begin--Author:scott Date:20190805 for：暂时注释掉密码加密逻辑，有点问题
		// 前端密码加密，后端进行密码解密
		// password =
		// AesEncryptUtil.desEncrypt(sysLoginModel.getPassword().replaceAll("%2B",
		// "\\+")).trim();//密码解密
		// update-begin--Author:scott Date:20190805 for：暂时注释掉密码加密逻辑，有点问题

		// update-begin-author:taoyan date:20190828 for:校验验证码
//		String captcha = sysLoginModel.getCaptcha();
//		if (captcha == null) {
//			result.error500("验证码无效");
//			return result;
//		}
//		String lowerCaseCaptcha = captcha.toLowerCase();
//		String realKey = MD5Util.MD5Encode(lowerCaseCaptcha + sysLoginModel.getCheckKey(), "utf-8");
//		Object checkCode = redisUtil.get(realKey);
		// 当进入登录页时，有一定几率出现验证码错误 #1714
//		if (checkCode == null || !checkCode.toString().equals(lowerCaseCaptcha)) {
//			result.error500("验证码错误");
//			return result;
//		}
		// update-end-author:taoyan date:20190828 for:校验验证码

		// 1. 校验用户是否有效
		// update-begin-author:wangshuai date:20200601 for: 登录代码验证用户是否注销bug，if条件永远为false
		LambdaQueryWrapper<SysUser> queryWrapper = new LambdaQueryWrapper<>();
		queryWrapper.eq(SysUser::getUsername, username);
		SysUser sysUser = sysUserService.getOne(queryWrapper);
		// update-end-author:wangshuai date:20200601 for: 登录代码验证用户是否注销bug，if条件永远为false
//		result = sysUserService.checkUserIsEffective(sysUser);
//		if (!result.isSuccess()) {
//			return result;
//		}

		// 2. 校验用户名或密码是否正确
		String userpassword = PasswordUtil.encrypt(username, password, sysUser.getSalt());
//		log.debug("-------------userpassword--------------------" + userpassword);
		String syspassword = sysUser.getPassword();
		if (!syspassword.equals(userpassword)) {
			result.error500("用户名或密码错误");
			return result;
		}

		// 用户登录信息
		userInfo(sysUser, result);
		// update-begin--Author:liusq Date:20210126 for：登录成功，删除redis中的验证码
//		redisUtil.del(realKey);
		// update-begin--Author:liusq Date:20210126 for：登录成功，删除redis中的验证码
//		LoginUser loginUser = new LoginUser();
//		BeanUtils.copyProperties(sysUser, loginUser);
//		baseCommonService.addLog("用户名: " + username + ",登录成功！", CommonConstant.LOG_TYPE_1, null,loginUser);
		// update-end--Author:wangshuai Date:20200714 for：登录日志没有记录人员
		return result;
	}
	
	private Result<JSONObject> userInfo(SysUser sysUser, Result<JSONObject> result) {
		String syspassword = sysUser.getPassword();
		String username = sysUser.getUsername();
		// 获取用户部门信息
		JSONObject obj = new JSONObject();
//		LoginUser userInfo = commonAPI.getUserByName(username);
		LoginUser userInfo = sysUserService.getLoginUserByName(username);
//		if (userInfo.getTenant() == null) {
//			result.error500("与该用户关联的租户均已被冻结，无法登录！");
//			return result;
//		}
//		obj.put("tenantList", Arrays.asList(userInfo.getTenant()));
		// update-end--Author:sunjianlei Date:20210802 for：获取用户租户信息
		// 生成token
		String token = JwtUtil.sign(username, syspassword, JSON.toJSONString(userInfo));
		// 设置token缓存有效时间
		redisUtil.set(CommonConstant.PREFIX_USER_TOKEN + token, token);
		redisUtil.expire(CommonConstant.PREFIX_USER_TOKEN + token, JwtUtil.EXPIRE_TIME * 2 / 1000);
		obj.put("token", token);
		obj.put("userInfo", sysUser);
//		obj.put("sysAllDictItems", sysDictService.queryAllDictItems());
		result.setResult(obj);
		result.success("登录成功");
		return result;
	}
	
//	public static void main(String[] args) {
//		System.out.println(PasswordUtil.encrypt("lixinAdmin", "123456", "RCGTeGiH"));
//	}

}