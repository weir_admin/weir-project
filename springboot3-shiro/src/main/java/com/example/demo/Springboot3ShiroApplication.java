package com.example.demo;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Springboot3ShiroApplication {

	public static void main(String[] args) {
		SpringApplication.run(Springboot3ShiroApplication.class, args);
	}
	
	@Bean
	public ExecutorService bootstrapExecutor() {
		return Executors.newCachedThreadPool();
	}

}
