package com.example.demo.system.serivce.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.common.constant.CommonConstant;
import com.example.demo.common.system.vo.LoginUser;
import com.example.demo.system.entity.SysPermission;
import com.example.demo.system.entity.SysTenant;
import com.example.demo.system.entity.SysUser;
import com.example.demo.system.mapper.SysPermissionMapper;
import com.example.demo.system.mapper.SysUserMapper;
import com.example.demo.system.mapper.SysUserRoleMapper;
import com.example.demo.system.serivce.ISysUserService;
import com.example.demo.utils.oConvertUtils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @Author: scott
 * @Date: 2018-12-20
 */
@Service
@Slf4j
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {
	
	@Autowired
	private SysUserMapper userMapper;
	@Autowired
	private SysPermissionMapper sysPermissionMapper;
	@Autowired
	private SysUserRoleMapper sysUserRoleMapper;
	
	@Override
	public LoginUser getLoginUserByName(String username) {
		if(oConvertUtils.isEmpty(username)) {
			return null;
		}
		LoginUser loginUser = new LoginUser();
		SysUser sysUser = userMapper.getUserByName(username);
		if(sysUser==null) {
			return null;
		}
		String tenantIds = sysUser.getRelTenantIds();
//		if (oConvertUtils.isNotEmpty(tenantIds)) {
//			List<String> tenantIdList = Arrays.asList(tenantIds.split(","));
//			// 该方法仅查询有效的租户，如果返回0个就说明所有的租户均无效。
//			LambdaQueryWrapper<SysTenant> queryWrapper = new LambdaQueryWrapper<>();
//	        queryWrapper.in(SysTenant::getId, tenantIdList);
//	        queryWrapper.eq(SysTenant::getStatus, CommonConstant.STATUS_1);
//			List<SysTenant> tenantList = sysTenantMapper.selectList(queryWrapper);
//			if (!CollectionUtils.isEmpty(tenantList)) {
//				sysUser.setRelTenantIds(tenantList.get(0).getDataSourceId().toString());
//				sysUser.setTenantLogo(tenantList.get(0).getLogo());
//				sysUser.setTenantName(tenantList.get(0).getName());
//				sysUser.setTenant(tenantList.get(0));
//			}
//		}
		BeanUtils.copyProperties(sysUser, loginUser);
//		TenantVo tenantVo = new TenantVo();
//		BeanUtils.copyProperties(sysUser.getTenant(), tenantVo);
//		loginUser.setTenant(tenantVo);
		List<String> roleSet = sysUserRoleMapper.getRoleByUserName(username);
		// 设置用户拥有的权限集合，比如“sys:role:add,sys:user:add”
		Set<String> permissionSet = new HashSet<>();
		List<SysPermission> permissionList = sysPermissionMapper.queryByUser(username);
		for (SysPermission po : permissionList) {
			if (oConvertUtils.isNotEmpty(po.getPerms())) {
				permissionSet.add(po.getPerms());
			}
		}
		loginUser.setRoleSet(new HashSet<>(roleSet));
		loginUser.setPermissionSet(permissionSet);
		if (permissionSet.contains("stock:stock")) {
			loginUser.setStockModel(true);
		}
		return loginUser;
	}

}
