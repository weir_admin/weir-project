package com.example.demo.system.serivce;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.common.system.vo.LoginUser;
import com.example.demo.system.entity.SysUser;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @Author scott
 * @since 2018-12-20
 */
public interface ISysUserService extends IService<SysUser> {

	LoginUser getLoginUserByName(String username);

}
