package com.example.demo.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

/**
 * 
* @Title: SysTenantPermission
* @Description: 租户权限
* @author: 深圳云邦互联-weir
* @date 2022年2月19日 下午5:41:43
 */
@Data
@TableName("sys_tenant_permission")
public class SysTenantPermission {

	private String id;
	private String tenantd;
	private String permissiond;
	
}
