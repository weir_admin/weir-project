package com.example.demo.common.system.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import java.io.Serializable;
import java.util.Date;

/**
 * 租户信息
 */
@Data
public class TenantVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编码
     */
    private Integer id;
    
    /**
     * 名称
     */
    private String name;
    

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 创建时间
     */
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 开始时间
     */
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date beginDate;

    /**
     * 结束时间
     */
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date endDate;

    /**
     * 状态 1正常 0冻结
     */
    @Dict(dicCode = "tenant_status")
    private Integer status;
    /**
     * 数据源
     */
    @Dict(dictTable ="saas_data_source",dicText = "name",dicCode = "id")
    private Integer dataSourceId;
    /**
     * 租户LOGO
     */
    private String logo;
    private String address;
    private String contactInfo;
    
    @TableField(exist = false)
    private String dataSource;
    
    /**
     * 联系人
     */
    private String contactor;

}
