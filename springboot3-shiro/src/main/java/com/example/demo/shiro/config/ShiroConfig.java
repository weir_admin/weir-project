//package com.example.demo.shiro.config;
//import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
//import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import com.example.demo.UserRealm;
//
//import java.util.LinkedHashMap;
//import java.util.Map;
//
//@Configuration
//public class ShiroConfig {
//
//    @Bean(name = "shiroFilterFactoryBean")
//    public ShiroFilterFactoryBean getShiroFilterFactoryBean(@Qualifier("defaultWebSecurityManager") DefaultWebSecurityManager securityManager) {
//        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
//
//        shiroFilterFactoryBean.setSecurityManager(securityManager);
//
//        Map<String, String> filterMap = new LinkedHashMap<String, String>();
//
//        filterMap.put("/update", "anon");
//
//        filterMap.put("/add", "authc");
//
////        filterMap.put("/*", "authc");
//        filterMap.put("/hello", "anon");
//
//        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterMap);
//        return shiroFilterFactoryBean;
//    }
//
//    @Bean(name = "defaultWebSecurityManager")
//    public DefaultWebSecurityManager getDefaultWebSecurityManager(@Qualifier("userRealm") UserRealm userRealm) {
//
//        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
//
//        securityManager.setRealm(userRealm);
//        return securityManager;
//    }
//
//    @Bean(name = "userRealm")
//    public UserRealm getRealm() {
//        return new UserRealm();
//    }
//}
