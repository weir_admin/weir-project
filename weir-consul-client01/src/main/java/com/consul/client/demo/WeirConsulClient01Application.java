package com.consul.client.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@EnableDiscoveryClient
@SpringBootApplication
@OpenAPIDefinition(info =
@Info(title = "client01 API", version = "${springdoc.version}", description = "Documentation client01 API v1.0")
)
@EnableFeignClients
public class WeirConsulClient01Application {

	public static void main(String[] args) {
		SpringApplication.run(WeirConsulClient01Application.class, args);
	}

}
