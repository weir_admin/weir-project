package com.consul.client.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("client01")
public class HelloController {

	@GetMapping("get")
	public String name() {
		return "client01";
	}
	
	@PostMapping("post")
	public String postName() {
		return "client01";
	}
}
