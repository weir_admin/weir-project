package com.weir.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.weir.user.entity.PermRole;

@Component
public interface RoleRepository extends JpaRepository<PermRole, Integer> {
	PermRole findByName(String name);
}