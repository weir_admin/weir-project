package com.weir.user.controller;

import com.weir.user.entity.PermUser;
import com.weir.user.entity.PermUserDTO;
import com.weir.user.jwt.JWTUtil;
import com.weir.user.repository.UserRepository;
import com.weir.user.service.UserService;
import com.weir.user.service.UserServiceFeign;
import com.weir.user.vo.ApiRespResult;
import com.weir.user.vo.LoginVo;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController implements UserServiceFeign{

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UserService userService;

//	@Operation(summary = "登录")
	@PostMapping("/login")
	public ApiRespResult<Object> login(@RequestBody LoginVo loginVo) {
		PermUser sysUsers = userService.getByUsername(loginVo.getUserName());
		if (sysUsers != null) {
			if (sysUsers.getPwd().equals(loginVo.getPwd())) {
				String token = JWTUtil.signAndAuths(sysUsers.getUsername(), sysUsers.getPwd(),StringUtils.arrayToCommaDelimitedString(sysUsers.getPermCodes().toArray()));
				sysUsers.setToken(token);
				
				return new ApiRespResult<>(sysUsers);
			} else {
				return ApiRespResult.error("密码错误");
			}
		} else {
			return ApiRespResult.error("该用户不存在");
		}
	}
	@GetMapping("/{id}")
	public PermUserDTO get(@PathVariable("id") Integer id) {
		PermUser u = userRepository.getReferenceById(id);
		if (u == null) {
			return null;
		}
		PermUserDTO userDTO = new PermUserDTO();
		BeanUtils.copyProperties(u, userDTO);
		return userDTO;
	}
	
//	@PreAuthorize("hasAuthority('user_add')")
	@GetMapping("get/{username}")
	public PermUserDTO getByName(@PathVariable("username") String username) {
		PermUser user = userService.getByUsername(username);
		if (user == null) {
			return null;
		}
		PermUserDTO userDTO = new PermUserDTO();
		BeanUtils.copyProperties(user, userDTO);
		return userDTO;
	}
}
