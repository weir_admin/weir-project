package com.weir.user.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.weir.user.entity.PermUser;
import com.weir.user.service.UserService;

@Component
public class JWTAuthenticationManager implements AuthenticationManager {

	@Autowired
	private UserService userService;

	/**
	 * 进行token鉴定
	 * 
	 * @param authentication 待鉴定的JWTAuthenticationToken
	 * @return 鉴定完成的JWTAuthenticationToken，供Controller使用
	 * @throws AuthenticationException 如果鉴定失败，抛出
	 */
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String token = authentication.getCredentials().toString();
		String username = JWTUtil.getUsername(token);
		PermUser sysUsers = userService.getByUsername(username);
		if (sysUsers == null) {
			throw new UsernameNotFoundException("该用户不存在");
		}
		boolean isAuthenticatedSuccess = JWTUtil.verify(token, username, sysUsers.getPwd());
		if (!isAuthenticatedSuccess) {
			throw new BadCredentialsException("用户名或密码错误");
		}
		String authCode = JWTUtil.getAuthCode(token);
		JWTAuthenticationToken authenticatedAuth = new JWTAuthenticationToken(token, sysUsers,
				AuthorityUtils.commaSeparatedStringToAuthorityList(authCode));
		return authenticatedAuth;
	}
}
