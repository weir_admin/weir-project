package com.weir.user.jwt;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

/**
 * @author weir
 *
 * 2019年6月20日 下午2:35:08
 */
public class JWTAuthenticationFilter extends BasicAuthenticationFilter {

	private static final String AUTHORIZATION = "Authorization";
	private static final String BEARER = "bearer ";
	private static final String SPACE = " ";
	
	/**
	 * 使用我们自己开发的JWTAuthenticationManager
	 * 
	 * @param authenticationManager 我们自己开发的JWTAuthenticationManager
	 */
	public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
		super(authenticationManager);
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		final String header = request.getHeader(AUTHORIZATION);
		if (header == null || !header.toLowerCase().startsWith(BEARER)) {
			chain.doFilter(request, response);
			return;
		}
		try {
			final String token = header.split(SPACE)[1];
			final JWTAuthenticationToken JWToken = new JWTAuthenticationToken(token);
			// 鉴定权限，如果鉴定失败，AuthenticationManager会抛出异常被我们捕获
			final Authentication authResult = getAuthenticationManager().authenticate(JWToken);
			// 将鉴定成功后的Authentication写入SecurityContextHolder中供后序使用
			SecurityContextHolder.getContext().setAuthentication(authResult);
		} catch (AuthenticationException failed) {
			SecurityContextHolder.clearContext();
			// 返回鉴权失败
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, failed.getMessage());
			return;
		}
		chain.doFilter(request, response);
	}
}
