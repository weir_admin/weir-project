package com.weir.user.config;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.source.ImmutableJWKSet;
import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.proc.SecurityContext;
import com.weir.user.jwt.JWTAuthenticationFilter;
import com.weir.user.jwt.JWTAuthenticationManager;

import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtEncoder;
import org.springframework.security.oauth2.server.resource.web.BearerTokenAuthenticationEntryPoint;
import org.springframework.security.oauth2.server.resource.web.access.BearerTokenAccessDeniedHandler;

@Configuration
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
//@EnableMethodSecurity
public class SecurityConfig {

	@Autowired
	private JWTAuthenticationManager jwtAuthenticationManager;
	
	@Value("${jwt.public.key}")
	RSAPublicKey key;

	@Value("${jwt.private.key}")
	RSAPrivateKey priv;

	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
		// @formatter:off
		http
		.csrf().disable()
		.authorizeRequests()
		.requestMatchers(HttpMethod.OPTIONS, "/**").permitAll()
//		.anyRequest().permitAll()
		.requestMatchers("/actuator/**").permitAll()
		.requestMatchers("/user/login").permitAll()
		.requestMatchers("/user/login-ui").permitAll()
		.requestMatchers("/user/get/**").permitAll()
//		.requestMatchers("/send_sms_code").permitAll()
//		.requestMatchers("/check_sms_code").permitAll()
//		.requestMatchers("/update_pwd").permitAll()
//		.requestMatchers("/init_perm").permitAll()
//		.requestMatchers("/huanbao/**").permitAll()
		// swagger start
		.requestMatchers("/swagger-ui/").permitAll().requestMatchers("/swagger-resources/**").permitAll()
		.requestMatchers("/swagger-ui.html").permitAll().requestMatchers("/swagger-resources/**").permitAll()
		.requestMatchers("/images/**").permitAll().requestMatchers("/webjars/**").permitAll()
		.requestMatchers("/v2/api-docs").permitAll().requestMatchers("/configuration/ui").permitAll()
		.requestMatchers("/v3/api-docs").permitAll().requestMatchers("/configuration/ui").permitAll()
		.requestMatchers("/configuration/security").permitAll()
		.requestMatchers("/v3/api-docs/**").permitAll()
		.requestMatchers("/swagger-ui/**").permitAll()
		.anyRequest().authenticated().and()
				.addFilterAt(new JWTAuthenticationFilter(jwtAuthenticationManager),
						UsernamePasswordAuthenticationFilter.class)
				.httpBasic(Customizer.withDefaults())
				.oauth2ResourceServer(OAuth2ResourceServerConfigurer::jwt)
				.sessionManagement((session) -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
				.exceptionHandling((exceptions) -> exceptions
						.authenticationEntryPoint(new BearerTokenAuthenticationEntryPoint())
						.accessDeniedHandler(new BearerTokenAccessDeniedHandler())
				);
		// @formatter:on
		return http.build();
	}

	@Bean
	JwtDecoder jwtDecoder() {
		return NimbusJwtDecoder.withPublicKey(this.key).build();
	}

	@Bean
	JwtEncoder jwtEncoder() {
		JWK jwk = new RSAKey.Builder(this.key).privateKey(this.priv).build();
		JWKSource<SecurityContext> jwks = new ImmutableJWKSet<>(new JWKSet(jwk));
		return new NimbusJwtEncoder(jwks);
	}
	
}
