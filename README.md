# weir-project--spring cloud 最新技术研究

#### 项目介绍
{spring cloud 最新技术研究。spring cloud 生态、阿里生态(Nacos dubbo rocketmq)、华为生态(Apache ServiceComb,Apache ServiceComb Saga),
最实战例子研究，分享实战经验，完全开源，少理论多实践，拿出代码说话，这就是该开源项目的目的，所有技术绝不会有任何形式的收费。}

#### 软件架构
目前有maven，jdk17，spring boot 3.0.0-RC1，spring cloud 2022.0.0-RC1，mysql8.x（更多技术请查看maven）
 
例子有：

1. eureka-client（eureka客户端）rc 可用

2. eureka-server（eureka服务端，注册中心）rc 可用

~~3. gateway-zuul（zuul 网管）不再支持~~

4. security-base 实现 jwt+rbac+springdoc-ui

5. weir-consul-client01 weir-consul-client02 实现consul + OpenFeign

#### 安装教程

1. 不用多说，启动注册中心之后再启动各个服务
2. 关于数据库resources文件里面都有脚本

#### 使用说明

1. 欢迎大家提交代码，各分支说明技术点即可
2. 想成为开发者请点评留言申请（写清楚码云账号即可）
3. oauth2授权码模式步骤：
   
   3.1. 请求：http://localhost:8081/oauth/authorize?response_type=code&client_id=weir&redirect_uri=http://www.loveweir.com
       获得code
   
   3.2. 请求：http://localhost:8081/oauth/token?
        grant_type=authorization_code&code=EAV5A3&redirect_uri=http://www.loveweir.com&scope=all
        得到授权码：{
"access_token": "255862f2-15d7-4bf8-8750-8c686b11ea52",
"token_type": "bearer",
"expires_in": 7199,
"scope": "all"
}
   
   3.3. 利用授权码，放在请求的header里面请求资源。

#### 参与贡献

1. 任何人都可以参与
2. 每个人提交的代码必须是自己的分支
3. master合并分支必须在分支上经过测试
4. 任何人不得以任何借口做收费，所有分支代码全网共享


交流群：weirweiwei(备注spring交流)