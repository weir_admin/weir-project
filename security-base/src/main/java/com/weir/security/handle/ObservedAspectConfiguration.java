package com.weir.security.handle;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.micrometer.observation.ObservationRegistry;
import io.micrometer.observation.aop.ObservedAspect;

@Configuration
public class ObservedAspectConfiguration {

  @Bean
  public ObservedAspect observedAspect(ObservationRegistry observationRegistry) {
    observationRegistry.observationConfig().observationHandler(new SimpleLoggingHandler());

    return new ObservedAspect(observationRegistry);
  }
}