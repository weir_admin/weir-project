package com.weir.security.config;

import java.io.IOException;
import java.nio.file.Files;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.JwtValidators;
import org.springframework.security.oauth2.jwt.MappedJwtClaimSetConverter;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.source.ImmutableJWKSet;
import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.proc.SecurityContext;
import com.weir.security.jwt.JWTAuthenticationFilter;
import com.weir.security.jwt.JWTAuthenticationManager;

// 开启方法注解功能
//@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
@EnableMethodSecurity
@Configuration
public class SecurityConfig {

	@Value("${jwt.public.key}")
	RSAPublicKey key;

	@Value("${jwt.private.key}")
	RSAPrivateKey priv;
	
	@Autowired
    private RestTemplateBuilder restTemplateBuilder;
	
	@Autowired
	private JWTAuthenticationManager jwtAuthenticationManager;

	@Bean
	SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		// restful具有先天的防范csrf攻击，所以关闭这功能
		http
		.csrf(AbstractHttpConfigurer::disable)
		.authorizeHttpRequests((request) ->request
			.requestMatchers(HttpMethod.OPTIONS, "/**").permitAll()
			.requestMatchers("/actuator/**").permitAll()
			.requestMatchers("/login").permitAll()
			.requestMatchers("/weir/**").permitAll()
			.requestMatchers("/send_sms_code").permitAll()
			.requestMatchers("/check_sms_code").permitAll()
			.requestMatchers("/update_pwd").permitAll()
			.requestMatchers("/init_perm").permitAll()
			.requestMatchers("/upload_one").permitAll()
			.requestMatchers("/video/**").permitAll()
			.requestMatchers("/download/**").permitAll()
			// swagger start
			.requestMatchers("/swagger-ui/").permitAll().requestMatchers("/swagger-resources/**").permitAll()
			.requestMatchers("/swagger-ui.html").permitAll().requestMatchers("/swagger-resources/**").permitAll()
			.requestMatchers("/images/**").permitAll().requestMatchers("/webjars/**").permitAll()
			.requestMatchers("/v2/api-docs").permitAll().requestMatchers("/configuration/ui").permitAll()
			.requestMatchers("/v3/api-docs").permitAll().requestMatchers("/configuration/ui").permitAll()
			.requestMatchers("/configuration/security").permitAll()
			.requestMatchers("/v3/api-docs/**").permitAll()
			.requestMatchers("/swagger-ui/**").permitAll()
			// swagger end
			.requestMatchers("/*/*.html", "/*/*.jpg", "/*/*.svg", "/*/*.ico", "/*/*.mp4","/*/*.css", "/*/*.js", "/*/*.map", 
					"/*/*.png", "/static/**","/*/*.js", "/*/*.json","/js/**", "/fonts/**", "/img/**", "/css/**").permitAll()
			.anyRequest().authenticated()
			)
				// 添加属于我们自己的过滤器，注意因为我们没有开启formLogin()，所以UsernamePasswordAuthenticationFilter根本不会被调用
				.addFilterAt(new JWTAuthenticationFilter(jwtAuthenticationManager),
						UsernamePasswordAuthenticationFilter.class)
				// 前后端分离本身就是无状态的，所以我们不需要cookie和session这类东西。所有的信息都保存在一个token之中。
				.sessionManagement(manager -> manager.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .oauth2ResourceServer((oauth2ResourceServer) -> oauth2ResourceServer
                		.jwt((jwt) -> jwt
                				.decoder(jwtDecoder(restTemplateBuilder))
                				)
                		)
                ;
		return http.build();
	}
	
//	@Bean
//	public WebSecurityCustomizer webSecurityCustomizer() {
//		return (web) -> web.ignoring().requestMatchers("/static/**","/*/*.js", "/*/*.json",
//				"/*/*.css", "/*/*.js", "/*/*.map", "/*/*.png", 
//				"/*/*.html", "/*/*.jpg", "/*/*.svg", "/*/*.ico", "/*/*.mp4");
//	}
	
	/**
     * jwt 的解码器
     *
     * @return JwtDecoder
     */
    public JwtDecoder jwtDecoder(RestTemplateBuilder builder) {
        // 方式一：通过 jwks url 构建
        // NimbusJwtDecoder.JwkSetUriJwtDecoderBuilder jwtDecoderBuilder = NimbusJwtDecoder.withJwkSetUri("http://qq.com:8080/oauth2/jwks");

        // 方式二：通过收取服务器的公钥创建
        // 授权服务器 jwk 的信息
		NimbusJwtDecoder decoder = null;
		try {
			Resource resource = new FileSystemResource("/Users/weir/git/weir-project/security-base/public-key.pem");
			String publicKeyStr = String.join("", Files.readAllLines(resource.getFile().toPath()));
			byte[] publicKeyBytes = Base64.getDecoder().decode(publicKeyStr);
			X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicKeyBytes);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			RSAPublicKey rsaPublicKey = (RSAPublicKey)keyFactory.generatePublic(keySpec);
			NimbusJwtDecoder.PublicKeyJwtDecoderBuilder jwtDecoderBuilder = NimbusJwtDecoder.withPublicKey(rsaPublicKey);

			decoder = jwtDecoderBuilder
			        // 设置获取 jwk 信息的超时时间
			        // .restOperations(
			        //         builder.setReadTimeout(Duration.ofSeconds(3))
			        //                 .setConnectTimeout(Duration.ofSeconds(3))
			        //                 .build()
			        // )
			        .build();
			// 对jwt进行校验
			decoder.setJwtValidator(JwtValidators.createDefault());
			// 对 jwt 的 claim 中增加值
			decoder.setClaimSetConverter(
			        MappedJwtClaimSetConverter.withDefaults(Collections.singletonMap("为claim中增加key", custom -> "值"))
			);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
        return decoder;
    }

	@Bean
	JwtEncoder jwtEncoder() {
		JWK jwk = new RSAKey.Builder(this.key).privateKey(this.priv).build();
		JWKSource<SecurityContext> jwks = new ImmutableJWKSet<>(new JWKSet(jwk));
		return new NimbusJwtEncoder(jwks);
	}
}
