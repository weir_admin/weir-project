package com.weir.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableWebSecurity
@EnableTransactionManagement
@SpringBootApplication
public class SecurityBaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecurityBaseApplication.class, args);
	}

}
