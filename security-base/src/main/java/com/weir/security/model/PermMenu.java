package com.weir.security.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * <p>
 * 菜单
 * </p>
 *
 * @author weir
 * @since 2018-12-24
 */
@TableName(value="perm_menu")
public class PermMenu extends Model<PermMenu> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 模块名称
     */
    private String text;

    /**
     * 模块编码
     */
    private String code;

    /**
     * 模块路径
     */
    private String url;

    /**
     * 父级ID
     */
    private Integer pid;

    /**
     * 级别(1菜单和2功能)
     */
    private Integer step;

    /**
     * 备注
     */
    private String remark;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Integer getStep() {
        return step;
    }

    public void setStep(Integer step) {
        this.step = step;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
	public Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "PermMenu{" +
        "id=" + id +
        ", text=" + text +
        ", code=" + code +
        ", url=" + url +
        ", pid=" + pid +
        ", step=" + step +
        ", remark=" + remark +
        "}";
    }
}
