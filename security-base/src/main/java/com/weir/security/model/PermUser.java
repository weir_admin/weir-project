package com.weir.security.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
@TableName(value = "perm_user")
public class PermUser extends Model<PermUser> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String username;

    private String pwd;

    /**
	 * 用户角色集合
	 */
	@TableField(exist = false)
	private String roleCodes;
	/**
	 * 用户菜单权限集合
	 */
	@TableField(exist = false)
	private String moduleCodes;
	@TableField(exist = false)
	private String token;

    public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getRoleCodes() {
		return roleCodes;
	}

	public void setRoleCodes(String roleCodes) {
		this.roleCodes = roleCodes;
	}

	public String getModuleCodes() {
		return moduleCodes;
	}

	public void setModuleCodes(String moduleCodes) {
		this.moduleCodes = moduleCodes;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    @Override
	public Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "PermUser{" +
        "id=" + id +
        ", username=" + username +
        ", pwd=" + pwd +
        "}";
    }
}
