package com.weir.security.vo;

public class Json{

	private boolean success = false;

	/**
	 * 返回正确或错误消息
	 */
	private String msg = "";

	/**
	 * object可以是单个值也可以是一个对象或集合格式
	 */
	private Object obj = null;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}

}
