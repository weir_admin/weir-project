package com.weir.security.vo;

public class PermRoleVo {
	private Integer id;

    private String name;

    private String permissionIds;
    private String parentPermissionIds;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPermissionIds() {
		return permissionIds;
	}
	public void setPermissionIds(String permissionIds) {
		this.permissionIds = permissionIds;
	}
	public String getParentPermissionIds() {
		return parentPermissionIds;
	}
	public void setParentPermissionIds(String parentPermissionIds) {
		this.parentPermissionIds = parentPermissionIds;
	}
}
