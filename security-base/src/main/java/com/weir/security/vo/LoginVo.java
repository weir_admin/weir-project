package com.weir.security.vo;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 登录
 * @author weir
 *
 * 2019年5月15日 上午9:20:54
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class LoginVo {

	@NotBlank(message = "用户名不能为空")
	private String userName;
	
	@NotBlank(message = "密码不能为空")
	private String pwd;
}
