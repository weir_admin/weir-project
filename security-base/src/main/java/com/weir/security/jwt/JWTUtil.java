package com.weir.security.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Date;

import org.springframework.security.oauth2.jwt.JwtDecoder;

/**
 * @author weir
 *
 * 2019年6月20日 下午2:59:09
 */
public class JWTUtil {

	// 过期时间24 * 60分钟
//	private static final long EXPIRE_TIME = 10 * 1000;
	private static final long EXPIRE_TIME = 20 * 24 * 60 * 60 * 1000;
	private static final String USERNAME = "username";
	private static final String EXPIRE = "expiretime";
	
	/**
	 * 校验token是否正确
	 * 
	 * @param token  密钥
	 * @param secret 用户的密码
	 * @return 是否正确
	 */
	public static boolean verify(String token, String username, String secret) {
		try {
			Algorithm algorithm = Algorithm.HMAC256(secret);
			JWTVerifier verifier = JWT.require(algorithm).withClaim(USERNAME, username).build();
			verifier.verify(token);
			return true;
		} catch (Exception exception) {
			return false;
		}
	}

	/**
	 * 获得token中的信息无需secret解密也能获得
	 * 
	 * @return token中包含的用户名
	 */
	public static String getUsername(String token) {
		try {
			DecodedJWT jwt = JWT.decode(token);
			return jwt.getClaim(USERNAME).asString();
		} catch (JWTDecodeException e) {
			return null;
		}
	}
	/**
	 * 获得token中的信息无需secret解密也能获得
	 * 
	 * @return token中包含的到期时间
	 */
	public static Date getExpireTime(String token) {
		try {
			DecodedJWT jwt = JWT.decode(token);
			return jwt.getClaim(EXPIRE).asDate();
		} catch (JWTDecodeException e) {
			return null;
		}
	}

	/**
	 * 生成签名
	 * 
	 * @param username 用户名
	 * @param secret   用户的密码
	 * @return 加密的token
	 */
	public static String sign(String username, String secret) {
		Date date = new Date(System.currentTimeMillis() + EXPIRE_TIME);
		Algorithm algorithm = Algorithm.HMAC256(secret);
		// 附带username信息
		return JWT.create()
				.withClaim(USERNAME, username)
				.withClaim(EXPIRE, date)
				.withExpiresAt(date).sign(algorithm);
	}
	
	/**
	 * 判断是否过期
	 * @param token token
	 * @return boolean
	 */
	public static boolean checkExpire(String token) {
		Date nowDate = new Date();
		Date expireTime = getExpireTime(token);
		if (expireTime.before(nowDate)) {
			return true;
		}
		return false;
	}
	
	public static void main(String[] args) {
//		System.out.println(sign("weir", "123456"));
//		String sq = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmV0aW1lIjoxNTYxMDE3MDgxLCJleHAiOjE1NjEwMTcwODEsInVzZXJuYW1lIjoid2VpciJ9.IHLAtnFvyVxL61ocAfIb9_PClaJ_Hfxkrzw4hsorkAQ";
//		System.out.println(verify(sq, "weir", "123456"));
//		System.out.println(getUsername(sq));
//		System.out.println(getExpireTime(sq));
		String sign = sign("admin", "QEwd/DWmy/4yGncCqBofQQ==");
		System.out.println(sign);
		
//		String sw = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmV0aW1lIjoxNTU5MzM2NDY2LCJleHAiOjE1NTkzMzY0NjYsInVzZXJuYW1lIjoiYWRtaW4ifQ.ZuuC0sm06l1ku5jzrtnHBGTi4skcI0IswCMtEy2D4CM";
		System.out.println(getUsername(sign));
		System.out.println(getExpireTime(sign));
		System.out.println(verify(sign, "admin", "QEwd/DWmy/4yGncCqBofQQ=="));
	}
}
