package com.weir.security.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.weir.security.mapper.PermRolePermissionMapper;
import com.weir.security.model.PermRolePermission;
import com.weir.security.service.IPermRolePermissionService;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色权限表 服务实现类
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
@Service
public class PermRolePermissionServiceImpl extends ServiceImpl<PermRolePermissionMapper, PermRolePermission> implements IPermRolePermissionService {

}
