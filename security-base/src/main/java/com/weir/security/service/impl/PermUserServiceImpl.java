package com.weir.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.weir.security.mapper.PermUserMapper;
import com.weir.security.model.PermMenu;
import com.weir.security.model.PermUser;
import com.weir.security.model.PermUserRole;
import com.weir.security.service.IPermUserService;

import io.micrometer.observation.annotation.Observed;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
@Service
public class PermUserServiceImpl extends ServiceImpl<PermUserMapper, PermUser> implements IPermUserService {

	@Autowired
	private PermUserMapper permUserMapper;
	
	@Observed(name = "PermUserServiceImpl--getByName")
	@Override
	public PermUser getByName(String name) {
		PermUser sysUsers = getOne(new QueryWrapper<PermUser>().eq("username", name.trim()));
		if (sysUsers != null) {
			String userRoleCodes = permUserMapper
					.getUserRoleCodes(new QueryWrapper<PermUserRole>().eq("ur.user_id", sysUsers.getId()));
			sysUsers.setRoleCodes(userRoleCodes);
			String userModuleCodes = permUserMapper
					.getUserModuleCodes(new QueryWrapper<PermMenu>().eq("ur.user_id", sysUsers.getId()));
			sysUsers.setModuleCodes(userModuleCodes);
		}
		return sysUsers;
	}
	
	@Override
	public List<String> getRoleName(Integer userId) {
		return new ArrayList<String>(permUserMapper.selectRoleNameByUserId(userId));
	}
	
	@Override
	public List<String> getPermName(Integer userId) {
		return new ArrayList<String>(permUserMapper.selectPermNameByUserId(userId));
	}
	
	@Override
	public boolean exitUserByField(String field,String name) {
		if (permUserMapper.selectCount(new QueryWrapper<PermUser>().eq(field, name))>0) {
			return true;
		}
		return false;
	}
}
