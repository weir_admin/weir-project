package com.weir.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.weir.security.mapper.PermRoleMapper;
import com.weir.security.mapper.PermRolePermissionMapper;
import com.weir.security.model.PermRole;
import com.weir.security.model.PermRolePermission;
import com.weir.security.service.IPermRoleService;
import com.weir.security.vo.PermRoleVo;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
@Service
public class PermRoleServiceImpl extends ServiceImpl<PermRoleMapper, PermRole> implements IPermRoleService {
	@Autowired
	private PermRolePermissionMapper permRolePermissionMapper;
	@Autowired
	private PermRoleMapper permRoleMapper;

	@Override
	@Transactional
	public void del(Integer id) {
		permRolePermissionMapper.delete(new QueryWrapper<PermRolePermission>().eq("role_id", id));
		permRoleMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void insertOrUpdate(PermRoleVo role) {
		if (role.getId() != null) {
			permRolePermissionMapper.delete(new QueryWrapper<PermRolePermission>().eq("role_id", role.getId()));
		}
		PermRole permRole = new PermRole();
		BeanUtils.copyProperties(role, permRole);
		saveOrUpdate(permRole);
		if (StringUtils.hasText(role.getPermissionIds())) {
			String[] ids = role.getPermissionIds().split(",");
			for (String s : ids) {
				PermRolePermission rolemodule = new PermRolePermission(Integer.valueOf(s), permRole.getId());
				permRolePermissionMapper.insert(rolemodule);
			}

		}
	}
}
