package com.weir.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.weir.security.model.PermUserRole;

/**
 * <p>
 * 用户角色表 服务类
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
public interface IPermUserRoleService extends IService<PermUserRole> {

}
