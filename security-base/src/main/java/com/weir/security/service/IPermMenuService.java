package com.weir.security.service;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.weir.security.model.PermMenu;
import com.weir.security.vo.MenuVo;

/**
 * <p>
 * 菜单 服务类
 * </p>
 *
 * @author weir
 * @since 2018-12-24
 */
public interface IPermMenuService extends IService<PermMenu> {

	List<PermMenu> getModule(Wrapper<PermMenu> wrapper);

	void delete(Integer id);

	List<MenuVo> listTree();

	List<MenuVo> getTrees(Integer id, Integer userId);

}
