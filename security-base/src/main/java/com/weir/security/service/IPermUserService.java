package com.weir.security.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.weir.security.model.PermUser;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
public interface IPermUserService extends IService<PermUser> {

	List<String> getRoleName(Integer userId);

	List<String> getPermName(Integer userId);

	boolean exitUserByField(String field, String name);

	PermUser getByName(String name);

}
