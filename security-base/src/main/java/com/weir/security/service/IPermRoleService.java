package com.weir.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.weir.security.model.PermRole;
import com.weir.security.vo.PermRoleVo;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
public interface IPermRoleService extends IService<PermRole> {

	void del(Integer id);

	void insertOrUpdate(PermRoleVo role);

}
