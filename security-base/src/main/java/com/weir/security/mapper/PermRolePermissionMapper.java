package com.weir.security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.weir.security.model.PermRolePermission;

/**
 * <p>
 * 角色权限表 Mapper 接口
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
public interface PermRolePermissionMapper extends BaseMapper<PermRolePermission> {

}
