package com.weir.security.controller;

import java.util.Date;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.weir.security.jwt.JWTUtil;
import com.weir.security.model.PermUser;
import com.weir.security.service.IPermMenuService;
import com.weir.security.service.IPermRolePermissionService;
import com.weir.security.service.IPermUserService;
import com.weir.security.vo.ApiRespResult;
import com.weir.security.vo.LoginVo;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Tag(name = "登录管理")
@RestController
public class LoginController {

	@Autowired
	private IPermUserService permUserService;
//	@Autowired
//	private IPermMenuService permMenuService;
//	@Autowired
//	private IPermRolePermissionService permRolePermissionService;

	@Operation(summary = "登录")
	@PostMapping("/login")
	public ApiRespResult<Object> login(@RequestBody LoginVo loginVo) {
		PermUser sysUsers = permUserService.getOne(new QueryWrapper<PermUser>()
				.eq("username", loginVo.getUserName().trim()));
		if (sysUsers != null) {
			if (sysUsers.getPwd().equals(loginVo.getPwd())) {
				String token = JWTUtil.sign(sysUsers.getUsername(), sysUsers.getPwd());
				sysUsers.setToken(token);
				
				return new ApiRespResult<>(sysUsers);
			} else {
				return ApiRespResult.error("密码错误");
			}
		} else {
			return ApiRespResult.error("该用户不存在");
		}
	}

	@Operation(summary = "刷新token")
	@GetMapping("/refresh_token/{id}")
	public ApiRespResult<Object> refreshToken(@PathVariable Long id, String token) {
		PermUser sysUsers = permUserService.getById(id);
		if (sysUsers == null) {
			return ApiRespResult.error("用户不存在");
		}
		// 是否注销
//		if (sysUsers.getIsCancel()) {
//			return new ApiRespResult<>(HttpStatus.SC_INTERNAL_SERVER_ERROR, StaticStringUtil.USER_CANCEL);
//		}
		String username = JWTUtil.getUsername(token);
		// 判断账号对应关系
		if (!sysUsers.getUsername().equals(username)) {
			return ApiRespResult.error("该用户不存在");
		}
		final Date plusDay = DateUtils.setDays(JWTUtil.getExpireTime(token), 2);
		final Date nowDate = new Date();
		// 过期时间小于两天，更新token
		if (plusDay.after(nowDate)) {
			token = JWTUtil.sign(username, sysUsers.getPwd());
			sysUsers.setToken(token);
			return new ApiRespResult<>(sysUsers);
		}
		return ApiRespResult.ok();
	}

	public void logout() {

	}

}
