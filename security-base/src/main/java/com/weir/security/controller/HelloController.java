package com.weir.security.controller;

import io.micrometer.observation.annotation.Observed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.simple.JdbcClient;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.weir.security.model.PermUser;
import com.weir.security.service.IPermUserService;

@RestController
@RequestMapping("/weir")
public class HelloController {
	
	@Autowired
	private IPermUserService permUserService;
	
	@Autowired
	private JdbcClient jdbcClient;

//	@PreAuthorize("hasAuthority('resource-get')")
//	@PreAuthorize("hasAuthority('user_list')")
	@GetMapping("/get")
	public PermUser name(String name) {
		PermUser byName = permUserService.getByName(name);
		return byName;
	}
	@Observed(name="ss-test")
	@GetMapping("/test")
	public  String test(String ss){
		jdbcClient.sql("").query(PermUser.class).list();
		return ss;
	}
}
