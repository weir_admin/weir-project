package com.weir.security;

import java.sql.Types;
import java.util.Collections;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

public class CodeGen2 {

	public static void main(String[] args) {
		FastAutoGenerator.create("jdbc:mysql://122.112.188.94:3306/saas-erp?characterEncoding=UTF-8&useUnicode=true&useSSL=false&tinyInt1isBit=false&allowPublicKeyRetrieval=true&serverTimezone=Asia/Shanghai",
				"weir", "weir336393QQ")
	    .globalConfig(builder -> {
	        builder.author("weir") // 设置作者
	            .enableSpringdoc() // 开启 swagger 模式
	            .fileOverride() // 覆盖已生成文件
	            .outputDir("/Users/weir/bojinerp/source"); // 指定输出目录
	    })
	    .dataSourceConfig(builder -> builder.typeConvertHandler((globalConfig, typeRegistry, metaInfo) -> {
	        int typeCode = metaInfo.getJdbcType().TYPE_CODE;
	        if (typeCode == Types.SMALLINT) {
	            // 自定义类型转换
	            return DbColumnType.INTEGER;
	        }
	        return typeRegistry.getColumnType(metaInfo);

	    }))
	    .packageConfig(builder -> {
	        builder.parent("com.yunbang.erp") // 设置父包名
	            .moduleName("wom") // 设置父包模块名
	            .pathInfo(Collections.singletonMap(OutputFile.xml, "/Users/weir/bojinerp/source")); // 设置mapperXml生成路径
	    })
	    .strategyConfig(builder -> {
	        builder.entityBuilder().enableLombok();
//	        .addInclude("t_simple") // 设置需要生成的表名
//	            .addTablePrefix("t_", "c_"); // 设置过滤表前缀
	    })
	    .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
	    .execute();

	}

}
