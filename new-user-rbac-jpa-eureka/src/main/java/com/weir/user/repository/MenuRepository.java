package com.weir.user.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.weir.user.entity.PermMenu;

@Component
public interface MenuRepository extends JpaRepository<PermMenu, Integer> {
	Optional<PermMenu> findById(Integer id);
}