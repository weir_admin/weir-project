package com.weir.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@SpringBootApplication
@EnableJpaRepositories(value = "com.weir.user.repository")
@EnableDiscoveryClient
public class NewUserRBACEurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(NewUserRBACEurekaApplication.class, args);
	}

}
