package com.weir.user.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.weir.user.entity.PermUser;
import com.weir.user.entity.PermUserDTO;
import com.weir.user.repository.UserRepository;
import com.weir.user.service.UserService;
import com.weir.user.service.UserServiceFeign;

@RestController
@RequestMapping("/user")
public class UserController implements UserServiceFeign{

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UserService userService;
	
	@GetMapping("/{id}")
	public PermUserDTO get(@PathVariable("id") Integer id) {
		PermUser u = userRepository.getReferenceById(id);
		if (u == null) {
			return null;
		}
		PermUserDTO userDTO = new PermUserDTO();
		BeanUtils.copyProperties(u, userDTO);
		return userDTO;
	}
	
	@GetMapping("get/{username}")
	public PermUserDTO getByName(@PathVariable("username") String username) {
		PermUser user = userService.getByUsername(username);
		if (user == null) {
			return null;
		}
		PermUserDTO userDTO = new PermUserDTO();
		BeanUtils.copyProperties(user, userDTO);
		return userDTO;
	}
}
