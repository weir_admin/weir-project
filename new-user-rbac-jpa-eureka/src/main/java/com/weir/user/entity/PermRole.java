package com.weir.user.entity;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import jakarta.persistence.JoinColumn;
import lombok.Data;

/**
 * <p>
 * 角色表
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
@Entity
@Table(name = "perm_role")
@Data
public class PermRole {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private String code;

    @ManyToMany(cascade = CascadeType.ALL ,fetch = FetchType.LAZY)
    @JoinTable(name = "perm_role_permission", joinColumns = {@JoinColumn(name = "role_id",referencedColumnName = "id")},
    inverseJoinColumns = {@JoinColumn(name = "permission_id",referencedColumnName = "id")})
    private List<PermMenu> permissions;    // 角色 -- 权限关系,多对多关系
//    @ManyToMany(fetch = FetchType.LAZY,mappedBy = "roles")
//    private List<PermUser> userInfos;
}
