package com.weir.user.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.weir.user.entity.PermMenu;
import com.weir.user.entity.PermRole;
import com.weir.user.entity.PermUser;
import com.weir.user.repository.RoleRepository;
import com.weir.user.repository.UserRepository;
import com.weir.user.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private RoleRepository roleRepository;

	@Override
	public PermUser getByUsername(String username) {
		PermUser permUser = userRepository.findByUsername(username);
		if (permUser != null && !CollectionUtils.isEmpty(permUser.getRoles())) {
			List<String> permCodes = new ArrayList<>();
			for (PermRole permRole : permUser.getRoles()) {
				List<String> codes = permRole.getPermissions().stream().map(PermMenu::getCode).distinct()
						.collect(Collectors.toList());
				permCodes.addAll(codes);
			}
			permUser.setPermCodes(permCodes);
		}
		return permUser;
	}
}
