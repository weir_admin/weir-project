package com.weir.user.service;

import com.weir.user.entity.PermUser;

public interface UserService {

	PermUser getByUsername(String username);

}
