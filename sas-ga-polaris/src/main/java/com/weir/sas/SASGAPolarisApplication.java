package com.weir.sas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.weir.user.service")
@SpringBootApplication
public class SASGAPolarisApplication {

	public static void main(String[] args) {
		SpringApplication.run(SASGAPolarisApplication.class, args);
	}

}
