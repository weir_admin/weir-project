package com.weir.sas.oauth2.service;

import org.springframework.security.oauth2.server.authorization.settings.ClientSettings;

import com.weir.sas.oauth2.entity.OAuth2ClientSetting;

public interface OAuth2ClientSettingsService {

	ClientSettings getClientSettings(OAuth2ClientSetting clientSetting);
	
}
