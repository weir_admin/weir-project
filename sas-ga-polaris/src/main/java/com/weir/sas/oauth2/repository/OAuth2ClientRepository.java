package com.weir.sas.oauth2.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.weir.sas.oauth2.entity.OAuth2Client;

public interface OAuth2ClientRepository extends CrudRepository<OAuth2Client, Long> {

	List<OAuth2Client> findByRegisteredFalse();
	
}
