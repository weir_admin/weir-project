package com.weir.sas.oauth2.customizer.jwt.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;
import org.springframework.security.oauth2.server.authorization.token.JwtEncodingContext;
import org.springframework.util.CollectionUtils;

import com.weir.sas.oauth2.customizer.jwt.JwtCustomizerHandler;
import com.weir.sas.service.impl.AuthUser;

public class UsernamePasswordAuthenticationTokenJwtCustomizerHandler extends AbstractJwtCustomizerHandler {

	public UsernamePasswordAuthenticationTokenJwtCustomizerHandler(JwtCustomizerHandler jwtCustomizerHandler) {
		super(jwtCustomizerHandler);
	}

	@Override
	protected void customizeJwt(JwtEncodingContext jwtEncodingContext) {
		
		Authentication authentication = jwtEncodingContext.getPrincipal();
		AuthUser userPrincipal = (AuthUser)authentication.getPrincipal();
		Long userId = userPrincipal.getId();
		Set<String> authorities = userPrincipal.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.collect(Collectors.toSet());
		
		Map<String, Object> userAttributes = new HashMap<>();
		userAttributes.put("userId", userId);
		userAttributes.put("authorities", authorities); // 自定义权限集合
		
		Set<String> contextAuthorizedScopes = jwtEncodingContext.getAuthorizedScopes();
		
		JwtClaimsSet.Builder jwtClaimSetBuilder = jwtEncodingContext.getClaims();
		
		if (CollectionUtils.isEmpty(contextAuthorizedScopes)) {
			jwtClaimSetBuilder.claim(OAuth2ParameterNames.SCOPE, authorities);
//			jwtClaimSetBuilder.claim("authorities", authorities);
		}
		
		jwtClaimSetBuilder.claims(claims ->
			userAttributes.entrySet().stream()
			.forEach(entry -> claims.put(entry.getKey(), entry.getValue()))
		);
		
	}

	@Override
	protected boolean supportCustomizeContext(Authentication authentication) {
		return authentication != null && authentication instanceof UsernamePasswordAuthenticationToken;
	}
	
}
