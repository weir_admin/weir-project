//package com.weir.sas.service;
//
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.validation.annotation.Validated;
//
//import com.weir.sas.jpa.entity.UserPrincipal;
//
//@Validated
//public interface UserPrincipalService extends UserDetailsService {
//
//	@Override
//    UserPrincipal loadUserByUsername(String username);
//	
//}
