package com.weir.sas.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.validation.annotation.Validated;

import com.weir.sas.service.impl.AuthUser;

@Validated
public interface MyUserPrincipalService extends UserDetailsService {

	@Override
	AuthUser loadUserByUsername(String username);
	
}
