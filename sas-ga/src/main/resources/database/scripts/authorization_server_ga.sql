/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80030
 Source Host           : localhost:3306
 Source Schema         : authorization_server_ga

 Target Server Type    : MySQL
 Target Server Version : 80030
 File Encoding         : 65001

 Date: 13/12/2022 21:55:31
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for oauth2_authorization
-- ----------------------------
DROP TABLE IF EXISTS `oauth2_authorization`;
CREATE TABLE `oauth2_authorization` (
  `id` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `registered_client_id` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `principal_name` varchar(200) COLLATE utf8mb4_general_ci NOT NULL,
  `authorization_grant_type` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `authorized_scopes` varchar(1000) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `attributes` blob,
  `state` varchar(500) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `authorization_code_value` blob,
  `authorization_code_issued_at` timestamp NULL DEFAULT NULL,
  `authorization_code_expires_at` timestamp NULL DEFAULT NULL,
  `authorization_code_metadata` blob,
  `access_token_value` blob,
  `access_token_issued_at` timestamp NULL DEFAULT NULL,
  `access_token_expires_at` timestamp NULL DEFAULT NULL,
  `access_token_metadata` blob,
  `access_token_type` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `access_token_scopes` varchar(1000) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `oidc_id_token_value` blob,
  `oidc_id_token_issued_at` timestamp NULL DEFAULT NULL,
  `oidc_id_token_expires_at` timestamp NULL DEFAULT NULL,
  `oidc_id_token_metadata` blob,
  `refresh_token_value` blob,
  `refresh_token_issued_at` timestamp NULL DEFAULT NULL,
  `refresh_token_expires_at` timestamp NULL DEFAULT NULL,
  `refresh_token_metadata` blob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of oauth2_authorization
-- ----------------------------
BEGIN;
INSERT INTO `oauth2_authorization` (`id`, `registered_client_id`, `principal_name`, `authorization_grant_type`, `authorized_scopes`, `attributes`, `state`, `authorization_code_value`, `authorization_code_issued_at`, `authorization_code_expires_at`, `authorization_code_metadata`, `access_token_value`, `access_token_issued_at`, `access_token_expires_at`, `access_token_metadata`, `access_token_type`, `access_token_scopes`, `oidc_id_token_value`, `oidc_id_token_issued_at`, `oidc_id_token_expires_at`, `oidc_id_token_metadata`, `refresh_token_value`, `refresh_token_issued_at`, `refresh_token_expires_at`, `refresh_token_metadata`) VALUES ('0703efc9-1f88-4194-b5a2-de1dffa0b3d3', '2', 'user1', 'authorization_code', 'message.read', 0x7B2240636C617373223A226A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C654D6170222C226F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E636F72652E656E64706F696E742E4F4175746832417574686F72697A6174696F6E52657175657374223A7B2240636C617373223A226F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E636F72652E656E64706F696E742E4F4175746832417574686F72697A6174696F6E52657175657374222C22617574686F72697A6174696F6E557269223A22687474703A2F2F3132372E302E302E313A393030302F737072696E67617574687365727665722F6F61757468322F617574686F72697A65222C22617574686F72697A6174696F6E4772616E7454797065223A7B2276616C7565223A22617574686F72697A6174696F6E5F636F6465227D2C22726573706F6E736554797065223A7B2276616C7565223A22636F6465227D2C22636C69656E744964223A22617574686F72697A6174696F6E2D636F64652D636C69656E742D6964222C227265646972656374557269223A22687474703A2F2F7777772E6C6F7665776569722E636F6D222C2273636F706573223A5B226A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C65536574222C5B226D6573736167652E72656164225D5D2C227374617465223A2277656972222C226164646974696F6E616C506172616D6574657273223A7B2240636C617373223A226A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C654D6170222C22636F6E74696E7565223A22227D2C22617574686F72697A6174696F6E52657175657374557269223A22687474703A2F2F3132372E302E302E313A393030302F737072696E67617574687365727665722F6F61757468322F617574686F72697A653F726573706F6E73655F747970653D636F646526636C69656E745F69643D617574686F72697A6174696F6E2D636F64652D636C69656E742D69642673636F70653D6D6573736167652E726561642673746174653D776569722672656469726563745F7572693D687474703A2F2F7777772E6C6F7665776569722E636F6D26636F6E74696E75653D222C2261747472696275746573223A7B2240636C617373223A226A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C654D6170227D7D2C226A6176612E73656375726974792E5072696E636970616C223A7B2240636C617373223A226F72672E737072696E676672616D65776F726B2E73656375726974792E61757468656E7469636174696F6E2E557365726E616D6550617373776F726441757468656E7469636174696F6E546F6B656E222C22617574686F726974696573223A5B226A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C6552616E646F6D4163636573734C697374222C5B7B2240636C617373223A22636F6D2E776569722E7361732E6A70612E656E746974792E55736572417574686F72697479222C22617574686F72697479223A2241444D494E227D2C7B2240636C617373223A22636F6D2E776569722E7361732E6A70612E656E746974792E55736572417574686F72697479222C22617574686F72697479223A2255534552227D5D5D2C2264657461696C73223A7B2240636C617373223A226F72672E737072696E676672616D65776F726B2E73656375726974792E7765622E61757468656E7469636174696F6E2E57656241757468656E7469636174696F6E44657461696C73222C2272656D6F746541646472657373223A223132372E302E302E31222C2273657373696F6E4964223A224531364234353830414138313434323433313843354138463236454530313544227D2C2261757468656E74696361746564223A747275652C227072696E636970616C223A7B2240636C617373223A22636F6D2E776569722E7361732E6A70612E656E746974792E557365725072696E636970616C222C226964223A312C22757365726E616D65223A227573657231222C2268617368656450617373776F7264223A6E756C6C2C22656E61626C6564223A747275652C226163636F756E744E6F6E45787069726564223A747275652C2263726564656E7469616C734E6F6E45787069726564223A747275652C226163636F756E744E6F6E4C6F636B6564223A747275652C22617574686F726974696573223A5B226A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C65536574222C5B7B2240636C617373223A22636F6D2E776569722E7361732E6A70612E656E746974792E55736572417574686F72697479222C22617574686F72697479223A2241444D494E227D2C7B2240636C617373223A22636F6D2E776569722E7361732E6A70612E656E746974792E55736572417574686F72697479222C22617574686F72697479223A2255534552227D5D5D2C226175646974223A7B2240636C617373223A22636F6D2E776569722E7361732E6A70612E61756469742E417564697444656C6574656444617465222C22637265617465644279223A302C226372656174656444617465223A313637303834353332302E3030303030303030302C226C6173744D6F6469666965644279223A302C226C6173744D6F64696669656444617465223A313637303834353332302E3030303030303030302C2264656C6574656444617465223A6E756C6C7D7D2C2263726564656E7469616C73223A6E756C6C7D7D, NULL, 0x354F7A49624175725458362D33566466336C613569437231334939345066544461735A454A362D5237622D736D2D665050306F424D6E436E52796967417433394C5838524274464C4F683264413855477838447672517475777943435F34314C584154506D6941516A38654F66786C495746437045525968452D455A6D627767, '2022-12-13 07:37:10', '2022-12-13 07:42:10', 0x7B2240636C617373223A226A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C654D6170222C226D657461646174612E746F6B656E2E696E76616C696461746564223A66616C73657D, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for oauth2_authorization_consent
-- ----------------------------
DROP TABLE IF EXISTS `oauth2_authorization_consent`;
CREATE TABLE `oauth2_authorization_consent` (
  `registered_client_id` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `principal_name` varchar(200) COLLATE utf8mb4_general_ci NOT NULL,
  `authorities` varchar(1000) COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`registered_client_id`,`principal_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of oauth2_authorization_consent
-- ----------------------------
BEGIN;
INSERT INTO `oauth2_authorization_consent` (`registered_client_id`, `principal_name`, `authorities`) VALUES ('2', 'user1', 'SCOPE_message.read');
COMMIT;

-- ----------------------------
-- Table structure for oauth2_client
-- ----------------------------
DROP TABLE IF EXISTS `oauth2_client`;
CREATE TABLE `oauth2_client` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `client_id` varchar(200) COLLATE utf8mb4_general_ci NOT NULL,
  `client_id_issued_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `client_name` varchar(200) COLLATE utf8mb4_general_ci NOT NULL,
  `client_secret` varchar(200) COLLATE utf8mb4_general_ci NOT NULL,
  `client_secret_expires_at` timestamp NULL DEFAULT NULL,
  `authentication_method` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `authorization_grant_type` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `scopes` varchar(1000) COLLATE utf8mb4_general_ci NOT NULL,
  `redirect_uris` varchar(1000) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `registered` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL,
  `created_by` bigint NOT NULL DEFAULT '0',
  `updated_date` timestamp NULL DEFAULT NULL,
  `updated_by` bigint DEFAULT NULL,
  `deleted_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_oauth2_client_id_client_id` (`id`,`client_id`),
  UNIQUE KEY `UC_oauth2_client_client_id` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of oauth2_client
-- ----------------------------
BEGIN;
INSERT INTO `oauth2_client` (`id`, `client_id`, `client_id_issued_at`, `client_name`, `client_secret`, `client_secret_expires_at`, `authentication_method`, `authorization_grant_type`, `scopes`, `redirect_uris`, `registered`, `created_date`, `created_by`, `updated_date`, `updated_by`, `deleted_date`) VALUES (1, 'authorization-code-client-id', '2022-12-12 11:41:37', 'authorization-code-client-name', '{noop}secret1', NULL, 'client_secret_post', 'authorization_code,refresh_token', 'message.read,message.write', 'http://127.0.0.1:8080/springauthserverclient/authorized,http://www.loveweir.com', 1, '2022-12-12 11:41:37', 0, '2022-12-12 11:41:37', 0, NULL);
INSERT INTO `oauth2_client` (`id`, `client_id`, `client_id_issued_at`, `client_name`, `client_secret`, `client_secret_expires_at`, `authentication_method`, `authorization_grant_type`, `scopes`, `redirect_uris`, `registered`, `created_date`, `created_by`, `updated_date`, `updated_by`, `deleted_date`) VALUES (2, 'client-credentials-client-id', '2022-12-12 11:41:37', 'client-credentials-client-name', '{noop}secret2', NULL, 'client_secret_basic', 'client_credentials,refresh_token', 'message.read,message.write', NULL, 1, '2022-12-12 11:41:37', 0, '2022-12-12 11:41:37', 0, NULL);
INSERT INTO `oauth2_client` (`id`, `client_id`, `client_id_issued_at`, `client_name`, `client_secret`, `client_secret_expires_at`, `authentication_method`, `authorization_grant_type`, `scopes`, `redirect_uris`, `registered`, `created_date`, `created_by`, `updated_date`, `updated_by`, `deleted_date`) VALUES (3, 'password-client-id', '2022-12-12 11:41:37', 'password-client-name', '{noop}secret3', NULL, 'client_secret_post', 'password,refresh_token', 'message.read,message.write', NULL, 1, '2022-12-12 11:41:37', 0, '2022-12-12 11:41:37', 0, NULL);
COMMIT;

-- ----------------------------
-- Table structure for oauth2_client_setting
-- ----------------------------
DROP TABLE IF EXISTS `oauth2_client_setting`;
CREATE TABLE `oauth2_client_setting` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `client_id` varchar(200) COLLATE utf8mb4_general_ci NOT NULL,
  `require_authorization_consent` tinyint(1) DEFAULT NULL,
  `created_date` timestamp NOT NULL,
  `created_by` bigint NOT NULL DEFAULT '0',
  `updated_date` timestamp NULL DEFAULT NULL,
  `updated_by` bigint DEFAULT NULL,
  `deleted_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_oauth2_client_setting_client_id` (`client_id`),
  CONSTRAINT `FK_oauth2_client_setting_client_id` FOREIGN KEY (`client_id`) REFERENCES `oauth2_client` (`client_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of oauth2_client_setting
-- ----------------------------
BEGIN;
INSERT INTO `oauth2_client_setting` (`id`, `client_id`, `require_authorization_consent`, `created_date`, `created_by`, `updated_date`, `updated_by`, `deleted_date`) VALUES (1, 'authorization-code-client-id', 1, '2022-12-12 11:41:37', 0, '2022-12-12 11:41:37', 0, NULL);
COMMIT;

-- ----------------------------
-- Table structure for oauth2_client_token_setting
-- ----------------------------
DROP TABLE IF EXISTS `oauth2_client_token_setting`;
CREATE TABLE `oauth2_client_token_setting` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `client_id` varchar(200) COLLATE utf8mb4_general_ci NOT NULL,
  `access_token_time` int NOT NULL,
  `access_token_time_unit` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `refresh_token_time` int NOT NULL,
  `refresh_token_time_unit` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `created_date` timestamp NOT NULL,
  `created_by` bigint NOT NULL DEFAULT '0',
  `updated_date` timestamp NULL DEFAULT NULL,
  `updated_by` bigint DEFAULT NULL,
  `deleted_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_oauth2_client_token_setting_client_id` (`client_id`),
  CONSTRAINT `FK_oauth2_client_token_setting_client_id` FOREIGN KEY (`client_id`) REFERENCES `oauth2_client` (`client_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of oauth2_client_token_setting
-- ----------------------------
BEGIN;
INSERT INTO `oauth2_client_token_setting` (`id`, `client_id`, `access_token_time`, `access_token_time_unit`, `refresh_token_time`, `refresh_token_time_unit`, `created_date`, `created_by`, `updated_date`, `updated_by`, `deleted_date`) VALUES (1, 'authorization-code-client-id', 1, 'day', 4, 'day', '2022-12-12 11:41:37', 0, '2022-12-12 11:41:37', 0, NULL);
INSERT INTO `oauth2_client_token_setting` (`id`, `client_id`, `access_token_time`, `access_token_time_unit`, `refresh_token_time`, `refresh_token_time_unit`, `created_date`, `created_by`, `updated_date`, `updated_by`, `deleted_date`) VALUES (2, 'client-credentials-client-id', 1, 'hour', 4, 'hour', '2022-12-12 11:41:37', 0, '2022-12-12 11:41:37', 0, NULL);
INSERT INTO `oauth2_client_token_setting` (`id`, `client_id`, `access_token_time`, `access_token_time_unit`, `refresh_token_time`, `refresh_token_time_unit`, `created_date`, `created_by`, `updated_date`, `updated_by`, `deleted_date`) VALUES (3, 'password-client-id', 30, 'minute', 1, 'day', '2022-12-12 11:41:37', 0, '2022-12-12 11:41:37', 0, NULL);
COMMIT;

-- ----------------------------
-- Table structure for oauth2_registered_client
-- ----------------------------
DROP TABLE IF EXISTS `oauth2_registered_client`;
CREATE TABLE `oauth2_registered_client` (
  `id` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `client_id` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `client_id_issued_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `client_secret` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `client_secret_expires_at` timestamp NULL DEFAULT NULL,
  `client_name` varchar(200) COLLATE utf8mb4_general_ci NOT NULL,
  `client_authentication_methods` varchar(1000) COLLATE utf8mb4_general_ci NOT NULL,
  `authorization_grant_types` varchar(1000) COLLATE utf8mb4_general_ci NOT NULL,
  `redirect_uris` varchar(1000) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `scopes` varchar(1000) COLLATE utf8mb4_general_ci NOT NULL,
  `client_settings` varchar(2000) COLLATE utf8mb4_general_ci NOT NULL,
  `token_settings` varchar(2000) COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of oauth2_registered_client
-- ----------------------------
BEGIN;
INSERT INTO `oauth2_registered_client` (`id`, `client_id`, `client_id_issued_at`, `client_secret`, `client_secret_expires_at`, `client_name`, `client_authentication_methods`, `authorization_grant_types`, `redirect_uris`, `scopes`, `client_settings`, `token_settings`) VALUES ('1', 'client-credentials-client-id', '2022-12-12 06:37:18', '{noop}secret1', NULL, 'client-credentials-client-name', 'client_secret_basic', 'refresh_token,client_credentials', '', 'message.read,message.write', '{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"settings.client.require-proof-key\":false,\"settings.client.require-authorization-consent\":false}', '{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"settings.token.reuse-refresh-tokens\":true,\"settings.token.id-token-signature-algorithm\":[\"org.springframework.security.oauth2.jose.jws.SignatureAlgorithm\",\"RS256\"],\"settings.token.access-token-time-to-live\":[\"java.time.Duration\",86400.000000000],\"settings.token.access-token-format\":{\"@class\":\"org.springframework.security.oauth2.server.authorization.settings.OAuth2TokenFormat\",\"value\":\"self-contained\"},\"settings.token.refresh-token-time-to-live\":[\"java.time.Duration\",345600.000000000],\"settings.token.authorization-code-time-to-live\":[\"java.time.Duration\",300.000000000]}');
INSERT INTO `oauth2_registered_client` (`id`, `client_id`, `client_id_issued_at`, `client_secret`, `client_secret_expires_at`, `client_name`, `client_authentication_methods`, `authorization_grant_types`, `redirect_uris`, `scopes`, `client_settings`, `token_settings`) VALUES ('2', 'authorization-code-client-id', '2022-12-12 06:37:18', '{noop}secret2', NULL, 'authorization-code-client-name', 'client_secret_post', 'refresh_token,authorization_code', 'http://127.0.0.1:8080/springauthserverclient/authorized,http://www.loveweir.com', 'message.read,message.write', '{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"settings.client.require-proof-key\":false,\"settings.client.require-authorization-consent\":true}', '{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"settings.token.reuse-refresh-tokens\":true,\"settings.token.id-token-signature-algorithm\":[\"org.springframework.security.oauth2.jose.jws.SignatureAlgorithm\",\"RS256\"],\"settings.token.access-token-time-to-live\":[\"java.time.Duration\",86400.000000000],\"settings.token.access-token-format\":{\"@class\":\"org.springframework.security.oauth2.server.authorization.settings.OAuth2TokenFormat\",\"value\":\"self-contained\"},\"settings.token.refresh-token-time-to-live\":[\"java.time.Duration\",345600.000000000],\"settings.token.authorization-code-time-to-live\":[\"java.time.Duration\",300.000000000]}');
INSERT INTO `oauth2_registered_client` (`id`, `client_id`, `client_id_issued_at`, `client_secret`, `client_secret_expires_at`, `client_name`, `client_authentication_methods`, `authorization_grant_types`, `redirect_uris`, `scopes`, `client_settings`, `token_settings`) VALUES ('3', 'password-client-id', '2022-12-12 06:37:18', '{noop}secret3', NULL, 'password-client-name', 'client_secret_post', 'refresh_token,password', '', '', '{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"settings.client.require-proof-key\":false,\"settings.client.require-authorization-consent\":false}', '{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"settings.token.reuse-refresh-tokens\":true,\"settings.token.id-token-signature-algorithm\":[\"org.springframework.security.oauth2.jose.jws.SignatureAlgorithm\",\"RS256\"],\"settings.token.access-token-time-to-live\":[\"java.time.Duration\",86400.000000000],\"settings.token.access-token-format\":{\"@class\":\"org.springframework.security.oauth2.server.authorization.settings.OAuth2TokenFormat\",\"value\":\"self-contained\"},\"settings.token.refresh-token-time-to-live\":[\"java.time.Duration\",345600.000000000],\"settings.token.authorization-code-time-to-live\":[\"java.time.Duration\",300.000000000]}');
COMMIT;

-- ----------------------------
-- Table structure for UserPrincipal
-- ----------------------------
DROP TABLE IF EXISTS `UserPrincipal`;
CREATE TABLE `UserPrincipal` (
  `UserId` bigint NOT NULL AUTO_INCREMENT,
  `Username` varchar(36) COLLATE utf8mb4_general_ci NOT NULL,
  `HashedPassword` varchar(128) COLLATE utf8mb4_general_ci NOT NULL,
  `AccountNonExpired` tinyint(1) NOT NULL,
  `AccountNonLocked` tinyint(1) NOT NULL,
  `CredentialsNonExpired` tinyint(1) NOT NULL,
  `Enabled` tinyint(1) NOT NULL,
  `created_date` timestamp NOT NULL,
  `created_by` bigint NOT NULL DEFAULT '0',
  `updated_date` timestamp NULL DEFAULT NULL,
  `updated_by` bigint DEFAULT NULL,
  `deleted_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`UserId`),
  UNIQUE KEY `UC_UserPrincipal_Username` (`Username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of UserPrincipal
-- ----------------------------
BEGIN;
INSERT INTO `UserPrincipal` (`UserId`, `Username`, `HashedPassword`, `AccountNonExpired`, `AccountNonLocked`, `CredentialsNonExpired`, `Enabled`, `created_date`, `created_by`, `updated_date`, `updated_by`, `deleted_date`) VALUES (1, 'user1', '{bcrypt}$2a$12$JG6r8yi2yHSYHNgoaQHJOeEhTS9uKavwaNWiNaEFXGVfpJU4l4MIe', 1, 1, 1, 1, '2022-12-12 11:42:00', 0, '2022-12-12 11:42:00', 0, NULL);
COMMIT;

-- ----------------------------
-- Table structure for UserPrincipalAuthority
-- ----------------------------
DROP TABLE IF EXISTS `UserPrincipalAuthority`;
CREATE TABLE `UserPrincipalAuthority` (
  `UserId` bigint NOT NULL,
  `Authority` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  UNIQUE KEY `UC_UserPrincipalAuthority_User_Authority` (`UserId`,`Authority`),
  CONSTRAINT `FK_UserPrincipalAuthority_UserId` FOREIGN KEY (`UserId`) REFERENCES `UserPrincipal` (`UserId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of UserPrincipalAuthority
-- ----------------------------
BEGIN;
INSERT INTO `UserPrincipalAuthority` (`UserId`, `Authority`) VALUES (1, 'ADMIN');
INSERT INTO `UserPrincipalAuthority` (`UserId`, `Authority`) VALUES (1, 'USER');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
