package com.weir.sas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SASGAApplication {

	public static void main(String[] args) {
		SpringApplication.run(SASGAApplication.class, args);
	}

}
