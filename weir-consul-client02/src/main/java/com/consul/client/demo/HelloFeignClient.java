package com.consul.client.demo;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "client01-service")
public interface HelloFeignClient {

	@GetMapping("client01/get")
	String name();
}
