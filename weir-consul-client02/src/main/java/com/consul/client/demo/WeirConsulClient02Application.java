package com.consul.client.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication
@OpenAPIDefinition(info =
@Info(title = "client02 API", version = "${springdoc.version}", description = "Documentation client02 API v1.0")
)
public class WeirConsulClient02Application {

	public static void main(String[] args) {
		SpringApplication.run(WeirConsulClient02Application.class, args);
	}

}
